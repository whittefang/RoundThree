﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using EngineFang;
using EngineFang.Animation;
using EngineFang.Graphics;
using RoundThree.General;
using EngineFang.Input;
using Microsoft.Xna.Framework;
using RoundThree.Morrigan;
using RoundThree.EngineFang;
using EngineFang.ui;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using RoundThree.General.Enums;

namespace RoundThree {
	public class Game1 : Game {
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		Scene scene, uiScene;
		Rectangle centerMarker;

		public Game1() {
			graphics = new GraphicsDeviceManager(this);
			graphics.PreferredBackBufferWidth = 1920;
			graphics.PreferredBackBufferHeight = 1080;
			Content.RootDirectory = "Content";
		}

		protected override void Initialize() {
			base.Initialize();
		}

		protected override void LoadContent() {
			Camera.Init(GraphicsDevice.Viewport, 4.5f);
			Camera.SetPosition(new Vector2(0, 100));
			SoundEffect.MasterVolume = .3f;
			LevelInformation.FloorPosition = 0;
			LevelInformation.Width = 170;

			Entity morrigan;
			Entity morrigan2;
			Entity ui, ui2;

			Entity shadow1, shadow2;

			scene = new Scene();
			uiScene = new Scene();
			ui = new Entity();
			ui2 = new Entity();
			centerMarker = new Rectangle(-5, 5, 10, 10);

			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);


			Entity level = VanityLevel.Load(Content, scene);

			PlayerUI playerUi =  UI.GetNewPlayerUI(ui, true, Content);
			PlayerUI playerUi2 = UI.GetNewPlayerUI(ui2, false, Content);


			

			morrigan = MorriganAttacks.NewInstance(Content, 0, scene, ui);

			morrigan2 = MorriganAttacks.NewInstance(Content, 1, scene, ui2);

			Sprite sprite = new Sprite(Content.Load<Texture2D>("characters/Shadow"), new Color(25, 25, 25, 150));
			shadow1 = new Entity();
			shadow1.addComponent(new SpriteRenderer(sprite));
			shadow1.addComponent(new ObjectFollower(morrigan));
			shadow1.transform.Position = new Vector2(0, -40);
			shadow2 = new Entity();
			shadow2.addComponent(new SpriteRenderer(sprite));
			shadow2.addComponent(new ObjectFollower(morrigan2));
			shadow2.transform.Position = new Vector2(0, -40);

			HitsparkFactory factory = new HitsparkFactory(scene);
			ObjectPool pool = new ObjectPool(factory);

			Dictionary<HitSpark, SpriteAnimation> animations = new Dictionary<HitSpark, SpriteAnimation>();

			SpriteAnimation animation = new SpriteAnimation(AnimationType.Single);
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/small/m_15000-0")), 3));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/small/m_15000-1")), 3));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/small/m_15000-2")), 3));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/small/m_15000-3")), 3));
			animation.AddFrame(new SpriteAnimationFrame(null, 3));

			SpriteAnimation animationMedium = new SpriteAnimation(AnimationType.Single);
			animationMedium.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/medium/m_15001-0")), 3));
			animationMedium.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/medium/m_15001-1")), 3));
			animationMedium.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/medium/m_15001-2")), 3));
			animationMedium.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/medium/m_15001-3")), 3));
			animationMedium.AddFrame(new SpriteAnimationFrame(null, 3));

			SpriteAnimation animationBlock = new SpriteAnimation(AnimationType.Single);
			animationBlock.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/block/m_15005-0")), 3));
			animationBlock.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/block/m_15005-1")), 3));
			animationBlock.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/block/m_15005-2")), 3));
			animationBlock.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/block/m_15005-3")), 3));
			animationBlock.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/block/m_15005-4")), 3));
			animationBlock.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/block/m_15005-5")), 3));
			animationBlock.AddFrame(new SpriteAnimationFrame(null, 3));

			SpriteAnimation animationHeavy = new SpriteAnimation(AnimationType.Single);
			animationHeavy.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/Heavy/m_15002-0")), 3));
			animationHeavy.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/Heavy/m_15002-1")), 3));
			animationHeavy.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/Heavy/m_15002-2")), 3));
			animationHeavy.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/Heavy/m_15002-3")), 3));
			animationHeavy.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("hitsparks/Heavy/m_15002-4")), 3));
			animationHeavy.AddFrame(new SpriteAnimationFrame(null, 3));

			animations.Add(HitSpark.light, animation);
			animations.Add(HitSpark.heavy, animationMedium);
			animations.Add(HitSpark.block, animationBlock);

			HitsparksPool.SetAnimations(animations);
			HitsparksPool.SetObjectPool(pool);

			CameraMover cameraMover = new CameraMover(morrigan, morrigan2);
			Entity camera = new Entity();
			camera.addComponent(cameraMover);

			//MediaPlayer.Play( Content.Load<Song>("sound/13 VANITY PARADISE"));
			//MediaPlayer.IsRepeating = true;
			RoundHandler.SetCameraMover(cameraMover);
			

			scene.addEntity(shadow1);
			scene.addEntity(shadow2);
			scene.addEntity(level);
			scene.addEntity(morrigan);
			scene.addEntity(morrigan2);
			scene.addEntity(camera);
			uiScene.addEntity(ui);
			uiScene.addEntity(ui2);
			uiScene.Load(Content);


			DebugControls.Load(Content);
			DebugControls.debugModeEnabled = true;
			UI.LoadUIArt(Content, uiScene);
			UI.Load(Content);

			RoundHandler.StartRound();
		}



		protected override void UnloadContent() {
		}

		protected override void Update(GameTime gameTime) {
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();
			HitstopController.Update();
			DebugControls.Update();
			if (!HitstopController.stopped && !DebugControls.pause) {
				scene.Update();
				uiScene.Update();
				RoundHandler.Update();
			}
			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime) {
			GraphicsDevice.Clear(Color.CornflowerBlue);

			spriteBatch.Begin(samplerState: SamplerState.PointClamp, transformMatrix: Camera.GetViewMatrix());
			scene.Draw(spriteBatch);
			DebugControls.Draw(spriteBatch);
			spriteBatch.Draw(DebugControls.debugSquareTexture, centerMarker, Color.Yellow);
			spriteBatch.End();

			spriteBatch.Begin(samplerState: SamplerState.PointClamp, transformMatrix: Camera.GetUIMatrix());
			uiScene.Draw(spriteBatch);
			spriteBatch.End();

			base.Draw(gameTime);
		}


	}
}
