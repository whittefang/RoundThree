﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.EngineFang.DataStructures {
	class Node<T> {
		private T data;
		private List<Node<T>> children;
		public Node() {
			children = new List<Node<T>>();
		}
		public Node(T data) {
			this.data = data;
			children = new List<Node<T>>();
		}

		public void Add(Node<T> node) {
			children.Add(node);
		}

		public List<Node<T>> GetChildren() {
			return children;
		}
		public T GetData() {
			return data;
		}
	}
}
