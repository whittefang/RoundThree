﻿using EngineFang.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.Animation
{
    class SpriteAnimationFrame
    {
        int frameLifetime;
        Sprite sprite;

        public SpriteAnimationFrame( Sprite sprite, int frameLifetime)
        {
            this.FrameLifetime = frameLifetime;
            this.Sprite = sprite;
        }

        public int FrameLifetime
        {
            get
            {
                return frameLifetime;
            }

            set
            {
                frameLifetime = value;
            }
        }

        public Sprite Sprite
        {
            get
            {
                return sprite;
            }

            set
            {
                sprite = value;
            }
        }
    }
}
