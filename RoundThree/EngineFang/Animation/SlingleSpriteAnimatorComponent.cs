﻿using EngineFang.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.Animation {
	class SingleSpriteAnimatorComponent : Component, Updatable {

		SingleSpriteAnimator animator;

		public SingleSpriteAnimatorComponent(SpriteRenderer renderer) {
			animator = new SingleSpriteAnimator(renderer);
		}

		public void PlayAnimation(SpriteAnimation animation) {
			animator.PlayAnimation(animation);
		}

		public void Update() {
			animator.Update();
		}
	}
}
