﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EngineFang.Graphics;

namespace EngineFang.Animation
{
    class SingleSpriteAnimator : ISpriteAnimator
    {
        bool isPlaying = false;
        int currentFrame;
        int currentFrameLifetime;
        List<SpriteAnimationFrame> frames;
        SpriteRenderer renderer;

        public SingleSpriteAnimator(SpriteRenderer renderer)
        {
            this.renderer = renderer;
        }

        public void PlayAnimation(SpriteAnimation animation)
        {
            isPlaying = true;
            currentFrame = 0;
            currentFrameLifetime = 0;
            frames = animation.Frames;
            renderer.Sprite = frames[0].Sprite;
        }

        public void StopAnimation()
        {
            isPlaying = false;
        }

        public void Update() {
            if (isPlaying) {
                UpdateFrame();
            }
            
        }

        private void UpdateFrame(){
            if (currentFrameLifetime >= frames[currentFrame].FrameLifetime)
            {
                currentFrame++;
                if (currentFrame >= frames.Count)
                {
                    isPlaying = false;
                }
                else {
                    renderer.Sprite = frames[currentFrame].Sprite;
                    currentFrameLifetime = 0;
                }
            }
			currentFrameLifetime++;
        }
    }
}
