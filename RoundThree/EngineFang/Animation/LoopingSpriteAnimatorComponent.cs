﻿using EngineFang.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.Animation {
	class LoopingSpriteAnimatorComponent: Component, Updatable {
		LoopingSpriteAnimator animator;

		public LoopingSpriteAnimatorComponent(SpriteRenderer renderer) {
			animator = new LoopingSpriteAnimator(renderer);
		}

		public void PlayAnimation(SpriteAnimation animation) {
			animator.PlayAnimation(animation);
		}

		public void Update() {
			animator.Update();
		}
	}
}
