﻿using EngineFang;
using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.Animation {
	class SpriteAnimator: Component, Updatable {
		Dictionary<String, SpriteAnimation> animations;
		Dictionary<AnimationType, ISpriteAnimator> animators;
		ISpriteAnimator currentAnimator;
		String currentAnimation;



		public SpriteAnimator(Dictionary<String, SpriteAnimation> animations, Dictionary<AnimationType, ISpriteAnimator> animators) {
			this.animations = animations;
			this.animators = animators;
		}

		public String CurrentAnimation {
			get {
				return currentAnimation;
			}
		}

		public void PlayAnimation(String animation) {
			SpriteAnimation selectedAnimation = animations[animation];
			PlayAnimation(selectedAnimation);
			currentAnimation = animation;
		}

		public void PlayAnimation(SpriteAnimation animation) {
			if (currentAnimator != null) {
				currentAnimator.StopAnimation();
			}
			currentAnimator = animators[animation.AnimationType];
			currentAnimator.PlayAnimation(animation);
			// fix current animation to give a better idea if play is inved this way
			currentAnimation = null;
		}

		public void StopAnimation() {
			if (currentAnimator != null) {
				currentAnimator.StopAnimation();
			}
			currentAnimator = null;
		}

		public void Update() {
			if (currentAnimator != null) {
				currentAnimator.Update();
			}
		}
	}
}
