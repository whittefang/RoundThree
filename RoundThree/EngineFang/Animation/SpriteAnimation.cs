﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.Animation {
	class SpriteAnimation {
		List<SpriteAnimationFrame> frames;
		AnimationType animationType;

		public SpriteAnimation(AnimationType animationType) {
			frames = new List<SpriteAnimationFrame>();
			this.AnimationType = animationType;
		}

		public void AddFrame(SpriteAnimationFrame frame) {
			frames.Add(frame);
		}


		public List<SpriteAnimationFrame> Frames {
			get {
				return frames;
			}
		}

		public AnimationType AnimationType {
			get {
				return animationType;
			}

			set {
				animationType = value;
			}
		}


	}
}
