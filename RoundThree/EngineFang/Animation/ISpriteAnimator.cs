﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.Animation
{
    interface ISpriteAnimator
    {
        void PlayAnimation(SpriteAnimation animation);
        void Update();
        void StopAnimation();
    }
}
