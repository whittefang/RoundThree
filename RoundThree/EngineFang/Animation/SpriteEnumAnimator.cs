﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.Animation
{
    class SpriteEnumAnimator<TEnum> : Component, Updatable
    {
        Dictionary<TEnum, SpriteAnimation> animations;
        Dictionary<AnimationType, ISpriteAnimator> animators;
        ISpriteAnimator currentAnimator;
        TEnum currentAnimation;

        

        public SpriteEnumAnimator(Dictionary<TEnum, SpriteAnimation> animations, Dictionary<AnimationType, ISpriteAnimator> animators)
        {
            this.animations = animations;
            this.animators = animators;
        }

        public TEnum CurrentAnimation
        {
            get
            {
                return currentAnimation;
            }
        }

        public void PlayAnimation(TEnum animation) {
            if (currentAnimator != null)
            {
                currentAnimator.StopAnimation();
            }
            SpriteAnimation selectedAnimation = animations[animation];
            currentAnimator = animators[selectedAnimation.AnimationType];
            currentAnimator.PlayAnimation(selectedAnimation);
            currentAnimation = animation;
        }

        public void StopAnimation() {
            currentAnimator.StopAnimation();
        }

        public void Update()
        {
            if (currentAnimator != null)
            {
                currentAnimator.Update();
            }
        }

    }
}
