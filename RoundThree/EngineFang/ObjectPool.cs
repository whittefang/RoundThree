﻿using EngineFang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.EngineFang {
	class ObjectPool {
		List<Entity> objects;
		EntityFactory factory;


		public ObjectPool(EntityFactory factory) {
			this.factory = factory;
			objects = new List<Entity>();
		}

		public Entity Get() {
			foreach (Entity entity in objects) {
				if (!entity.enabled) {
					entity.enabled = true;
					return entity;
				}
			}

			Entity newEntity =  factory.NewInstance();
			objects.Add(newEntity);

			return newEntity;
		}

	}
}
