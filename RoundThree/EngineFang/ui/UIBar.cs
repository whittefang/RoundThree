﻿using EngineFang;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoundThree.EngineFang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.ui {
	class UIBar: Component, Renderable {
		Texture2D barTexture;
		int  originalBarWidth;
		RectangleCollider currentBarRect;
		Color color;

		public UIBar(Texture2D barTexture, RectangleCollider originalBarRect, Color color) {
			this.barTexture = barTexture;
			this.originalBarWidth = originalBarRect.GetRectangle().Width;
			currentBarRect = originalBarRect;
			this.color = color;
		}

		public void ChangeFillAmount(float newFillAmount) {
			System.Console.WriteLine(newFillAmount);
			currentBarRect.Width = (int)(originalBarWidth * newFillAmount);
		}

		public void Draw(SpriteBatch spriteBatch) {
			spriteBatch.Draw(barTexture, currentBarRect.GetRectangle(), color);
		}
	}
}
