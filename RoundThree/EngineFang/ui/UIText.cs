﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.ui {
	class UIText: Component, Renderable {

		private SpriteFont font;
		private Color color;
		private Vector2 position;

		public bool Enabled {get; set;}

		public string Text {get; set;}

		public UIText(SpriteFont font,  Vector2 position, Color color = default(Color), string text = "", bool enabled = true) {
			this.font = font;
			this.Text = text;
			Enabled = enabled;
			this.color = color;
			this.position = position;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (Enabled) {
				spriteBatch.DrawString(font, Text, position, Color.White, 0, Vector2.Zero, .4f, SpriteEffects.None, 0);
			}
		}
	}
}
