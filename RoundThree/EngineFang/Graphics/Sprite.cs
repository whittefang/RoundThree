﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace EngineFang.Graphics
{
    class Sprite
    {
        Texture2D texture;
        Vector2 center;
		public Color Color { get; set; }
		public Sprite(Texture2D texture): this(texture, Color.White) {}

		public Sprite(Texture2D texture, Color color)
        {
            this.texture = texture;
            center = new Vector2(-texture.Bounds.Width / 2, texture.Bounds.Height/2);
			Color = color;
        }

        public Texture2D Texture
        {
            get
            {
                return texture;
            }
         
        }

        public Vector2 Center
        {
            get
            {
                return center;
            }
        }


    }
}
