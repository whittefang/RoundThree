﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace EngineFang.Graphics {
	class SpriteRenderer : Component, Renderable {
		private Sprite sprite;
		private SpriteEffects effect;
		public Vector2 Offset {
			get {
				return offset;
			}

			set {
				offset = value;
				adjustedOffset = value;
			}
		}
		private Vector2 offset;
		private Vector2 adjustedOffset;



		public Sprite Sprite {
			get { return sprite; }
			set { sprite = value; }
		}

		public SpriteRenderer() {}

		public SpriteRenderer(Sprite sprite) {
			this.sprite = sprite;
		}

		public void Flip(bool flip) {
			if (flip) {
				effect = SpriteEffects.FlipHorizontally;
				adjustedOffset = new Vector2(-Offset.X, Offset.Y);
			} else {
				effect = SpriteEffects.None;
				adjustedOffset = new Vector2(Offset.X, Offset.Y);
			}
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (sprite != null) {
				Vector2 renderPosition = transform.Position;
				renderPosition += Sprite.Center;
				renderPosition += adjustedOffset;
				renderPosition.Y = -renderPosition.Y;
				spriteBatch.Draw(Sprite.Texture, renderPosition, color: sprite.Color,effects: effect);
			}
		}
	}
}
