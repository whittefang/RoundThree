﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.EngineFang {
	class RectangleCollider {
		Rectangle rectangle;
		Vector2 offset;
		Vector2 position;

		public int Width {
			get {return rectangle.Width;}
			set {rectangle.Width = value;}
		}
		public int Height {
			get { return rectangle.Height; }
			set { rectangle.Height = value; }
		}
		public Vector2 Offset {
			get { return offset; }
			set { offset = value;
				SetPosition(position);
			}
		}

		public RectangleCollider(int x, int y, int width, int height) {
			rectangle = new Rectangle(0, 0, width, height);
			SetPosition(new Vector2(x, y));
			offset = Vector2.Zero;
		}
		public RectangleCollider(int width, int height) : this(0, 0, width, height) { }

		public void SetPosition(Vector2 newPosition) {
			this.position = newPosition;
			newPosition += Offset;

			// adjust for origin to be in the center
			newPosition.X = newPosition.X - rectangle.Width / 2;
			newPosition.Y = newPosition.Y + rectangle.Height / 2;

			// flip y axis to have positive axis be up
			newPosition.Y = -newPosition.Y;

			rectangle.X = (int)newPosition.X;
			rectangle.Y = (int)newPosition.Y;
		}
		public Vector2 GetPosition() {
			return position;
		}
		public Rectangle GetRectangle() {
			return rectangle;
		}
	}
}
