﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace EngineFang.Input
{
    class Input: Component, Updatable
    {
        int playerNumber;
        Dictionary<Buttons, VirtualButton> buttons;
        public delegate void BoolDel(Vector2 state);
        BoolDel leftThumbStick;

		bool enabled;

		GamePadState state, prevState;

		public bool Enabled {
			get {return enabled;}
			set {enabled = value;}
		}

		public Input(Dictionary<Buttons, VirtualButton> buttons, int playerNumber)
        {
            this.buttons = buttons;
            this.playerNumber = playerNumber;
            state = GamePad.GetState(playerNumber);
            prevState = state;
			Enabled = true;
        }

        public void Update()
        {
            prevState = state;
            state = GamePad.GetState(playerNumber);
			if (Enabled) {
				//TODO: genersize with virutal sticks for easy keybinding
				leftThumbStick(state.ThumbSticks.Left);

				foreach (Buttons button in buttons.Keys) {
					if (wasButtonPressed(button)) {
						buttons[button].PressButton();
					}
				}
			}
        }

        public void SetThumbstick(BoolDel newThumbstick) {
            leftThumbStick = newThumbstick;
        }

        private bool wasButtonPressed(Buttons button) {
            if (Enabled && state.IsButtonDown(button) && prevState.IsButtonUp(button)) {
                return true;
            }
            return false;
        }
        public Vector2 GetLeftThumbStick() {
			if (Enabled) {
				return state.ThumbSticks.Left;
			}
			return Vector2.Zero;
        }
    }
}
