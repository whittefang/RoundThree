﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang.Input
{
    class SingleFireVirtualButton: VirtualButton
    {
        public delegate bool BoolDel();
        private BoolDel action;

        public SingleFireVirtualButton(BoolDel action)
        {
            this.action = action;
        }

        public void PressButton() {
            action();
        }
    }
}
