﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EngineFang;
using Microsoft.Xna.Framework.Content;
using EngineFang.Graphics;
using EngineFang.Animation;
using RoundThree.General;

namespace RoundThree.EngineFang {
	class HitsparkFactory : EntityFactory {
		Scene scene;

		public HitsparkFactory(Scene scene) {
			this.scene = scene;
		}

		public Entity NewInstance() {
			Entity entity = new Entity();

			SpriteRenderer renderer = new SpriteRenderer();
			SingleSpriteAnimatorComponent animator = new SingleSpriteAnimatorComponent(renderer);
			TimedDisable timedDisable = new TimedDisable(15);

			entity.addComponent(animator);
			entity.addComponent(renderer);
			entity.addComponent(timedDisable);

			scene.addEntity(entity);
			return entity;
		}
	}
}
