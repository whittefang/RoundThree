﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace EngineFang
{
    class Transform
    {
        private Vector2 position;

        public Transform()
        {
            Position = Vector2.Zero;
        }

        public Transform(float x, float y)
        {
            position.X = x;
            position.Y = y;
        }

        public Vector2 Position
        {
            get
            {
                return position;
            }

            set
            {
                position = value;
            }
        }

        public void Translate(Vector2 amount)
        {
            Position += new Vector2(amount.X, amount.Y);
        }
    }
}
