﻿using EngineFang;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.EngineFang {
	class Scene {
		private List<Entity> entities;

		public Scene() {
			entities = new List<Entity>();
		}

		public void addEntity(Entity newEntity) {
			entities.Add(newEntity);
			newEntity.scene = this;
		}
		public void Load(ContentManager content) {
			foreach (Entity x in entities) {
				x.Load(content);
			}
		}

		public void Update() {
			for (int i = 0; i < entities.Count; i++) {
				entities[i].Update();
			}
		}
		public void Draw(SpriteBatch spriteBatch) {
			foreach (Entity x in entities) {
				x.Draw(spriteBatch);
			}
		}
	}
}
