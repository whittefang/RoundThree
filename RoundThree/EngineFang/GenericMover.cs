﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineFang {
	class GenericMover : Component, Updatable {
		public Vector2 Speed { get; set; }
		public Vector2 Direction { get; set; }

		public bool Active { get; set; }

		public GenericMover(Vector2 speed) : this(speed, new Vector2(1, 1)) { }
		public GenericMover(Vector2 speed, Vector2 direction) : this(speed, direction, true) { }
		public GenericMover(Vector2 speed, Vector2 direction, bool active) {
			Speed = speed;
			Direction = direction;
			Active = active;
		}



		public void Update() {
			if (Active) {
				transform.Position += Speed * Direction;
			}
		}
	}
}
