﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RoundThree.EngineFang;

namespace EngineFang
{
    class Entity
    {
        public Transform transform;
        public String id;
        public bool enabled;
        public Scene scene;
        List<Component> componentList;
        List<Updatable> updateableComponents;
        List<Renderable> renderableComponents;

        public Entity(String id = "")
        {
            componentList = new List<Component>();
            updateableComponents = new List<Updatable>();
            renderableComponents = new List<Renderable>();
            this.id = id;
            enabled = true;
            transform = new Transform(0, 0);
        }
        // add a new componenet into the entity
        public void addComponent(Component newComponent)
        {
            newComponent.Init(transform, this);
            componentList.Add(newComponent);
            if (newComponent is Updatable)
            {
                updateableComponents.Add(newComponent as Updatable);
            }
            if (newComponent is Renderable)
            {
                renderableComponents.Add(newComponent as Renderable);
            }
        }

        public T getComponent<T>() where T : Component
        {
            for (int i = 0; i < componentList.Count; i++)
            {
                if (componentList[i] is T)
                {
                    return componentList[i] as T;
                }
            }

            return null;
        }
        public List<T> getComponents<T>() where T : Component
        {
            List<T> results = new List<T>();
            for (int i = 0; i < componentList.Count; i++)
            {
                if (componentList[i] is T)
                {
                    results.Add(componentList[i] as T);
                }
            }

            return results;
        }

        public void Load(ContentManager content)
        {
            foreach (Component c in componentList)
            {
                c.Load(content);
            }
        }

        // push update to all updateable components
        public void Update()
        {
            if (enabled)
            {
                for (int i = 0; i < updateableComponents.Count; i++)
                {
                    updateableComponents[i].Update();
                }
            }
        }

        // push draw to all renderable components
        public void Draw(SpriteBatch spriteBatch)
        {
            if (enabled)
            {
                for (int i = 0; i < renderableComponents.Count; i++)
                {
                    renderableComponents[i].Draw(spriteBatch);
                }
            }
        }
    }
}
