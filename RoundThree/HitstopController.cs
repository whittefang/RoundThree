﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree {
	static class HitstopController {
		static int hitstopRemaining;
		public static bool stopped;

		public static void PlayHitstop(int duration) {
			if (duration > 0) {
				hitstopRemaining = duration;
				stopped = true;
			}
		}

		public static void Update() {
			if (stopped) {
				hitstopRemaining--;
				if (hitstopRemaining <= 0) {
					stopped = false;
				}
			}
		}

	}
}
