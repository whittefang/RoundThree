﻿using EngineFang;
using EngineFang.Animation;
using EngineFang.Graphics;
using EngineFang.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RoundThree.EngineFang;
using RoundThree.EngineFang.DataStructures;
using RoundThree.General;
using RoundThree.General.Attacks;
using RoundThree.General.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.Morrigan {
	class MorriganAttacks {

		public static Entity NewInstance(ContentManager Content, int playerNumber, Scene scene, Entity ui) {

			Entity morrigan = new Entity();
			Entity projectile = LoadProjectile(playerNumber, Content);
			Entity airProjectile = LoadAirProjectile(playerNumber, Content);
			PlayerMover mover;
			JumpPlayer jumpPlayer;

			HitResolver.SetProjectile(playerNumber, projectile.getComponent<Projectile>());
			HitResolver.SetProjectile(playerNumber, airProjectile.getComponent<Projectile>());

			PlayerState state = new PlayerState(playerNumber);
			SpriteRenderer spriteRenderer = new SpriteRenderer();
			spriteRenderer.Offset = new Vector2(0, 50);
			SingleSpriteAnimator ssAnimator = new SingleSpriteAnimator(spriteRenderer);
			LoopingSpriteAnimator loopingAnimator = new LoopingSpriteAnimator(spriteRenderer);
			AirRecoveryPlayer airRecoveryPlayer = new AirRecoveryPlayer();

			PushbackPlayer pushbackPlayer = new PushbackPlayer();

			Dictionary<Buttons, VirtualButton> moves = new Dictionary<Buttons, VirtualButton>();
			Input input = new Input(moves, playerNumber);

			Dictionary<String, SpriteAnimation> animations = new Dictionary<String, SpriteAnimation>();
			AttackPlayer attackPlayer = new AttackPlayer();
			Attack lightAttack;
			SpriteAnimator spriteAnimator;
			Dictionary<AnimationType, ISpriteAnimator> animators = new Dictionary<AnimationType, ISpriteAnimator>();
			spriteAnimator = new SpriteAnimator(animations, animators);
			CharacterAnimationController animatorController = new CharacterAnimationController(spriteAnimator, state);

			SpriteAnimation neutralAnim = new SpriteAnimation(AnimationType.Loop);
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (1)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (6)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (7)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (8)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (9)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (10)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (11)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (12)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (13)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (14)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (15)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (16)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (17)")), 3));
			neutralAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/neutral/m (18)")), 3));

			SpriteAnimation walkTowardAnim = new SpriteAnimation(AnimationType.Loop);
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (19)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (20)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (21)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (22)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (23)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (24)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (25)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (26)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (27)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (28)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (29)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (30)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (31)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (32)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (33)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (34)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (35)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (36)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (37)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (38)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (39)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (40)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (41)")), 1));
			walkTowardAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkforward/m (42)")), 1));


			SpriteAnimation walkBackAnim = new SpriteAnimation(AnimationType.Loop);
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (43)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (44)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (45)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (46)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (47)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (48)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (49)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (50)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (51)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (52)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (53)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (54)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (55)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (56)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (57)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (58)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (59)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (60)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (60)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (61)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (62)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (63)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (64)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (65)")), 3));
			walkBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/walkback/m (66)")), 3));


			SpriteAnimation airRecoveryAnim = new SpriteAnimation(AnimationType.Loop);
			airRecoveryAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (89)")), 3));
			airRecoveryAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (90)")), 3));
			airRecoveryAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (91)")), 3));


			SpriteAnimation crouchAnim = new SpriteAnimation(AnimationType.Single);
			crouchAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Crouch/m (119)")), 2));
			crouchAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Crouch/m (120)")), 2));
			crouchAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Crouch/m (121)")), 2));
			crouchAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Crouch/m (122)")), 2));

			SpriteAnimation dashTowardsAnim = new SpriteAnimation(AnimationType.Loop);
			dashTowardsAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/dashtoward/m (1300)")), 3));
			dashTowardsAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/dashtoward/m (1301)")), 3));

			SpriteAnimation dashBackAnim = new SpriteAnimation(AnimationType.Loop);
			dashBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/dashback/m (893)")), 3));
			dashBackAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/dashback/m (894)")), 3));

			SpriteAnimation hitAnim = new SpriteAnimation(AnimationType.Single);
			hitAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/hitstun/m (358)")), 3));
			hitAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/hitstun/m (359)")), 3));

			SpriteAnimation blockAnim = new SpriteAnimation(AnimationType.Single);
			blockAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Blockstun/m (132)")), 3));
			blockAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Blockstun/m (133)")), 3));
			blockAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Blockstun/m (134)")), 3));

			SpriteAnimation airHitAnim = new SpriteAnimation(AnimationType.Single);
			airHitAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (404)")), 3));
			airHitAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (405)")), 3));
			airHitAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (406)")), 3));
			airHitAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (407)")), 3));
			airHitAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (408)")), 3));
			
			SpriteAnimation knockdownAnim = new SpriteAnimation(AnimationType.Single);
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (409)")), 3));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (410)")), 3));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (411)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (485)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (486)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (487)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (488)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Getup/m (384)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Getup/m (385)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Getup/m (386)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Getup/m (387)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Getup/m (388)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Getup/m (389)")), 2));
			knockdownAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Getup/m (390)")), 2));


			SpriteAnimation deathAnim = new SpriteAnimation(AnimationType.Single);
			deathAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (409)")), 3));
			deathAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (410)")), 3));
			deathAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (411)")), 2));
			deathAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (485)")), 2));
			deathAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (486)")), 2));
			deathAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (487)")), 2));
			deathAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Knockdown/m (488)")), 2));

			SpriteAnimation jump = new SpriteAnimation(AnimationType.Single);
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (115)")), 2));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (80)")), 2));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (81)")), 2));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (82)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (83)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (84)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (85)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (86)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (87)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (88)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (89)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (90)")), 3));
			jump.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/jump/m (91)")), 3));

			SpriteAnimation heavyAnim = new SpriteAnimation(AnimationType.Single);
			heavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/heavy/m (678)")), 2));
			heavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/heavy/m (679)")), 2));
			heavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/heavy/m (680)")), 2));
			heavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/heavy/m (681)")), 2));
			heavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/heavy/m (682)")), 4));
			heavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/heavy/m (683)")), 6));
			heavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/heavy/m (684)")), 16));
			heavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/heavy/m (685)")), 4));

			SpriteAnimation axeKickAnim = new SpriteAnimation(AnimationType.Single);
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (628)")), 2));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (629)")), 2));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (630)")), 3));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (631)")), 3));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (632)")), 3));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (633)")), 3));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (634)")), 3));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (635)")), 3));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (636)")), 20));
			axeKickAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/axekick/m (637)")), 3));

			SpriteAnimation cHeavyAnim = new SpriteAnimation(AnimationType.Single);
			cHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cHeavy/m (734)")), 2));
			cHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cHeavy/m (735)")), 3));
			cHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cHeavy/m (736)")), 3));
			cHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cHeavy/m (737)")), 3));
			cHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cHeavy/m (738)")), 6));
			cHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cHeavy/m (739)")), 6));
			cHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cHeavy/m (740)")), 6));
			cHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cHeavy/m (741)")), 6));

			SpriteAnimation cLightAnim = new SpriteAnimation(AnimationType.Single);
			cLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cLight/m (745)")), 5));
			cLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cLight/m (746)")), 3));
			cLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cLight/m (747)")), 8));
			cLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/cLight/m (748)")), 7));

			SpriteAnimation jLightAnim = new SpriteAnimation(AnimationType.Single);
			jLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpLight/m (932)")), 2));
			jLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpLight/m (933)")), 2));
			jLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpLight/m (934)")), 2));
			jLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpLight/m (935)")), 3));
			jLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpLight/m (936)")), 3));
			jLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpLight/m (937)")), 3));
			jLightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpLight/m (938)")), 3));

			SpriteAnimation jHeavyAnim = new SpriteAnimation(AnimationType.Single);
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1302)")), 1));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1303)")), 2));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1304)")), 2));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1305)")), 2));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1306)")), 3));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1307)")), 3));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1308)")), 3));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1309)")), 3));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1310)")), 3));
			jHeavyAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/JumpHeavy/m (1311)")), 3));

			SpriteAnimation LightAnim = new SpriteAnimation(AnimationType.Single);
			LightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/light/m (649)")), 4));
			LightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/light/m (650)")), 3));
			LightAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/light/m (651)")), 10));

			SpriteAnimation lightString2Anim = new SpriteAnimation(AnimationType.Single);
			lightString2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain2/m (686)")), 5));
			lightString2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain2/m (687)")), 3));
			lightString2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain2/m (688)")), 10));

			SpriteAnimation lightString3Anim = new SpriteAnimation(AnimationType.Single);
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (655)")), 1));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (656)")), 2));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (657)")), 2));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (658)")), 2));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (659)")), 2));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (660)")), 2));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (661)")), 2));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (662)")), 2));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (663)")), 2));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (664)")), 10));
			lightString3Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/lightChain3/m (665)")), 4));

			SpriteAnimation sp1Anim = new SpriteAnimation(AnimationType.Single);
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (959)")), 3));
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (960)")), 3));
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (961)")), 2));
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (962)")), 2));
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (963)")), 2));
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (964)")), 2));
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (965)")), 2));
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (966)")), 34));
			sp1Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1/m (967)")), 2));

			SpriteAnimation sp1AirAnim = new SpriteAnimation(AnimationType.Single);
			sp1AirAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1Air/m (995)")), 3));
			sp1AirAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1Air/m (996)")), 3));
			sp1AirAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1Air/m (997)")), 5));
			sp1AirAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1Air/m (998)")), 3));
			sp1AirAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1Air/m (999)")), 3));
			sp1AirAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1Air/m (1000)")), 3));
			sp1AirAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1Air/m (1002)")), 3));
			sp1AirAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp1Air/m (1003)")), 3));
		

			SpriteAnimation sp2Anim = new SpriteAnimation(AnimationType.Single);
			sp2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp2/m (1074)")), 4));
			sp2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp2/m (1075)")), 3));
			sp2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp2/m (1076)")), 4));
			sp2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp2/m (1077)")), 20));
			sp2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp2/m (1080)")), 3));
			sp2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp2/m (1081)")), 3));
			sp2Anim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/sp2/m (1082)")), 3));

			
			SpriteAnimation winAnim = new SpriteAnimation(AnimationType.Single);
			winAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Win/m (173)")), 6));
			winAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Win/m (174)")), 6));
			winAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Win/m (175)")), 6));
			winAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Win/m (176)")), 6));
			winAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/Win/m (177)")), 6));

			Attack heavyAttack = new Attack(38);
			ActionFrame aFrame = new ActionFrame(9);
			aFrame.setAttack(new Hitbox(100, 35, 32, 25, new Vector2(-5, 0), new RectangleCollider(0, 0, 80, 70), new Vector2(65, 0), CancelState.light, General.Enums.HitSpark.heavy));
			heavyAttack.AddActionFrame(aFrame, 4);
			aFrame = new ActionFrame(9);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/heavyWhiff");
			heavyAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(15);
			aFrame.SetCancelFrame();
			heavyAttack.AddActionFrame(aFrame, 1);

			Attack axeKickAttack = new Attack(42);
			aFrame = new ActionFrame(10);
			aFrame.setAttack(new Hitbox(80, 25, 35, 25, new Vector2(-5, 0), new RectangleCollider(0, 0, 80, 20), new Vector2(65, 25), CancelState.light, General.Enums.HitSpark.heavy));
			axeKickAttack.AddActionFrame(aFrame, 5);
			aFrame = new ActionFrame(20);
			aFrame.SetCancelFrame();
			axeKickAttack.AddActionFrame(aFrame, 1);

			
			lightAttack = new Attack(14);
			aFrame = new ActionFrame(5);
			aFrame.setAttack(new Hitbox(15, 5, 14, 11, new Vector2(-5, 0), new RectangleCollider(0, 0, 45, 10), new Vector2(30, 33), CancelState.light, General.Enums.HitSpark.light));
			lightAttack.AddActionFrame(aFrame, 2);
			aFrame = new ActionFrame(5);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/lightWhiff");
			lightAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(12);
			aFrame.SetCancelFrame();
			lightAttack.AddActionFrame(aFrame, 1);

			Attack cHeavyAttack = new Attack(35);
			aFrame = new ActionFrame(9);
			aFrame.setAttack(new Hitbox(90, 30, 30, 7, new Vector2(-5, 10), new RectangleCollider(0, 0, 100, 15), new Vector2(75, -10), CancelState.light, General.Enums.HitSpark.light, AttackProperty.Launcher, 10));
			cHeavyAttack.AddActionFrame(aFrame, 3);
			aFrame = new ActionFrame(6);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/heavyWhiff");
			cHeavyAttack.AddActionFrame(aFrame, 1);

			Attack cLightAttack = new Attack(24);
			aFrame = new ActionFrame(6);
			aFrame.setAttack(new Hitbox(50, 20, 20, 14, new Vector2(-5, 0), new RectangleCollider(0, 0, 65, 20), new Vector2(45, -10), CancelState.light, General.Enums.HitSpark.light));
			cLightAttack.AddActionFrame(aFrame, 3);
			aFrame = new ActionFrame(6);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/lightWhiff");
			cLightAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(9);
			aFrame.SetCancelFrame();
			cLightAttack.AddActionFrame(aFrame, 1);

			Attack LightString2Attack = new Attack(14);
			aFrame = new ActionFrame(6);
			aFrame.setAttack(new Hitbox(20, 8, 16, 12, new Vector2(-5, 0), new RectangleCollider(0, 0, 40, 30), new Vector2(40, -20), CancelState.light, General.Enums.HitSpark.light));
			LightString2Attack.AddActionFrame(aFrame, 3);
			aFrame = new ActionFrame(6);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/lightWhiff");
			LightString2Attack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(12);
			aFrame.SetCancelFrame();
			LightString2Attack.AddActionFrame(aFrame, 1);

			Attack LightString3Attack = new Attack(31);
			aFrame = new ActionFrame(8);
			aFrame.setAttack(new Hitbox(50, 20, 25, 22, new Vector2(5, 0), new RectangleCollider(0, 0, 60, 10), new Vector2(70, 20), CancelState.light, General.Enums.HitSpark.heavy, pushbackDuration: 10, ignorePushback: true));
			LightString3Attack.AddActionFrame(aFrame, 3);
			aFrame = new ActionFrame(8);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/lightWhiff");
			LightString3Attack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(15);
			aFrame.SetCancelFrame();
			LightString3Attack.AddActionFrame(aFrame, 1);

			Attack sp1Attack = new Attack(46);
			aFrame = new ActionFrame(10);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/soulfist");
			sp1Attack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(16);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/soulFistProj");
			sp1Attack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(16);
			aFrame.SetOptionalFunction(() => {
				Vector2 offset = new Vector2(20 * attackPlayer.GetAttackDirection(), 15);
				projectile.transform.Position = morrigan.transform.Position + offset;
				GenericMover projMover =  projectile.getComponent<GenericMover>();
				if ((state.IsFacingRight && input.GetLeftThumbStick().X > .2f) || (!state.IsFacingRight && input.GetLeftThumbStick().X < -.2f)) {
					projMover.Speed = new Vector2(8, projMover.Speed.Y);
				} else if ((!state.IsFacingRight && input.GetLeftThumbStick().X > .2f) || (state.IsFacingRight && input.GetLeftThumbStick().X < -.2f)) {
					projMover.Speed = new Vector2(6, projMover.Speed.Y);
				} else {
					projMover.Speed = new Vector2(7, projMover.Speed.Y);
				}
				projectile.getComponent<Projectile>().Activate(state.IsFacingRight);
			});
			sp1Attack.AddActionFrame(aFrame, 1);
			

			

			Attack sp2Attack = new Attack(54);
			aFrame = new ActionFrame(4);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/srk");
			aFrame.SetOptionalFunction(()=> { state.CurrentState = FighterState.invincible; });
			sp2Attack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(5);
			aFrame.setAttack(new Hitbox(25, 10, 5, 7, new Vector2(0, 10), new RectangleCollider(0, 0, 35, 50), new Vector2(20, 0), CancelState.light, General.Enums.HitSpark.light, attackProperty: AttackProperty.Launcher, pushbackDuration: 10, ignorePushback: true));
			sp2Attack.AddActionFrame(aFrame, 3);
			aFrame = new ActionFrame(8);
			aFrame.setAttack(new Hitbox(25, 10, 5, 7, new Vector2(-2, 10), new RectangleCollider(0, 0, 30, 50), new Vector2(25, 10), CancelState.light, General.Enums.HitSpark.light, attackProperty: AttackProperty.Launcher, pushbackDuration: 15, ignorePushback: true));
			sp2Attack.AddActionFrame(aFrame, 3);
			aFrame = new ActionFrame(11);
			aFrame.setAttack(new Hitbox(50, 20, 50, 7, new Vector2(-5, 10), new RectangleCollider(0, 0, 30, 50), new Vector2(10, 60), CancelState.light, General.Enums.HitSpark.heavy, attackProperty: AttackProperty.Launcher, pushbackDuration: 15, ignorePushback: true));
			aFrame.setMovement(new Vector2(5, 10));
			sp2Attack.AddActionFrame(aFrame, 15);
			aFrame = new ActionFrame(27);
			aFrame.setMovement(new Vector2(3, -10));
			sp2Attack.AddActionFrame(aFrame, 15);
			aFrame = new ActionFrame(15);
			aFrame.SetOptionalFunction(() => { state.CurrentState = FighterState.attackStartup; });
			sp2Attack.AddActionFrame(aFrame, 1);

			Attack jLightAttack = new Attack(20, FighterState.jumpingAttack);
			aFrame = new ActionFrame(5);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/lightWhiff");
			jLightAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(5);
			aFrame.setAttack(new Hitbox(15, 5, 15, 7, new Vector2(-3, 0), new RectangleCollider(0, 0, 50, 20), new Vector2(40, 0), CancelState.light, General.Enums.HitSpark.heavy));
			jLightAttack.AddActionFrame(aFrame, 3);
			aFrame = new ActionFrame(8);
			aFrame.setAttack(new Hitbox(25, 10, 15, 7, new Vector2(-3, 0), new RectangleCollider(0, 0, 50, 20), new Vector2(40, 0), CancelState.light, General.Enums.HitSpark.heavy));
			jLightAttack.AddActionFrame(aFrame, 3);
			aFrame = new ActionFrame(11);
			aFrame.setAttack(new Hitbox(35, 15, 20, 7, new Vector2(-3, 0), new RectangleCollider(0, 0, 50, 20), new Vector2(40, 0), CancelState.light, General.Enums.HitSpark.heavy));
			jLightAttack.AddActionFrame(aFrame, 3);

			Attack jHeavyAttack = new Attack(25, FighterState.jumpingAttack);
			aFrame = new ActionFrame(8);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/lightWhiff");
			jHeavyAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(8);
			aFrame.setAttack(new Hitbox(70, 20, 25, 7, new Vector2(0, 0), new RectangleCollider(0, 0, 30, 70), new Vector2(0, 0), CancelState.light, General.Enums.HitSpark.heavy));
			jHeavyAttack.AddActionFrame(aFrame, 3);

			Attack jumpToward = BuildJump(5, attackPlayer);

			Attack jumpBack = BuildJump(-5, attackPlayer);

			Attack jumpNeutral = BuildJump(0, attackPlayer);

			Attack deathAttack = new Attack(999);


			Attack dashToward = new Attack(16);
			aFrame = new ActionFrame(2);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/Jet");
			dashToward.AddActionFrame(aFrame, 1);
			ActionFrame dashAction = new ActionFrame(2);
			dashAction.setMovement(new Vector2(10, 0));
			dashToward.AddActionFrame(dashAction, 10);

			Attack dashBack = new Attack(16);
			aFrame = new ActionFrame(2);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/Jet");
			dashBack.AddActionFrame(aFrame, 1);
			dashAction = new ActionFrame(2);
			dashAction.setMovement(new Vector2(-10, 0));
			dashBack.AddActionFrame(dashAction, 10);


			jumpPlayer = new JumpPlayer(animatorController, jumpNeutral, jumpToward, jumpBack);
			Health health = new Health(1000, ui.getComponent<PlayerUI>());

			Attack sp1AirAttack = new Attack(54);
			aFrame = new ActionFrame(10);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/soulfist");
			sp1AirAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(18);
			aFrame.optionalSound = Content.Load<SoundEffect>("sound/soulFistProj");
			sp1AirAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(1);
			aFrame.SetOptionalFunction(() => {
				jumpPlayer.cancelJump();
			});
			sp1AirAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(2);
			aFrame.setMovement(new Vector2(0, -1));
			sp1AirAttack.AddActionFrame(aFrame, 15);
			aFrame = new ActionFrame(18);
			aFrame.SetOptionalFunction(() => {
				Vector2 offset = new Vector2(35 * attackPlayer.GetAttackDirection(), 50);
				airProjectile.transform.Position = morrigan.transform.Position + offset;
				airProjectile.getComponent<Projectile>().Activate(state.IsFacingRight);
			});
			sp1AirAttack.AddActionFrame(aFrame, 1);
			aFrame = new ActionFrame(19);
			aFrame.setMovement(new Vector2(-5, 5));
			sp1AirAttack.AddActionFrame(aFrame, 5);
			aFrame = new ActionFrame(25);
			aFrame.setMovement(new Vector2(-4, 3));
			sp1AirAttack.AddActionFrame(aFrame, 2);
			aFrame = new ActionFrame(27);
			aFrame.setMovement(new Vector2(-3, 1));
			sp1AirAttack.AddActionFrame(aFrame, 2);
			aFrame = new ActionFrame(29);
			aFrame.setMovement(new Vector2(-1, 0));
			sp1AirAttack.AddActionFrame(aFrame, 2);
			aFrame = new ActionFrame(31); aFrame.SetOptionalFunction(() => {
				airRecoveryPlayer.Recover();
			});
			sp1AirAttack.AddActionFrame(aFrame, 1);

			animations.Add(CharacterAnimation.NEUTRAL, neutralAnim);
			animations.Add(CharacterAnimation.WALKTOWARD, walkTowardAnim);
			animations.Add(CharacterAnimation.WALKBACK, walkBackAnim);
			animations.Add(CharacterAnimation.CROUCH, crouchAnim);
			animations.Add(CharacterAnimation.JUMPNEUTRAL, jump);
			animations.Add(CharacterAnimation.DASHTOWARD, dashTowardsAnim);
			animations.Add(CharacterAnimation.DASHBACK, dashBackAnim);
			animations.Add(CharacterAnimation.BLOCK, blockAnim);
			animations.Add(CharacterAnimation.HIT, hitAnim);
			animations.Add(CharacterAnimation.SPECIALONE, sp1Anim);
			animations.Add(CharacterAnimation.SPECIALTWO, sp2Anim);
			animations.Add(CharacterAnimation.AIRRECOVERY, airRecoveryAnim);
			animations.Add(CharacterAnimation.AIRHIT, airHitAnim);
			animations.Add(CharacterAnimation.KNOCKDOWN, knockdownAnim);
			animations.Add(CharacterAnimation.DEATH, deathAnim);
			animations.Add(CharacterAnimation.WIN, winAnim);

			animators.Add(AnimationType.Single, ssAnimator);
			animators.Add(AnimationType.Loop, loopingAnimator);

			RectangleCollider movementCollider = new RectangleCollider(30, 30);
			movementCollider.Offset = new Vector2(0,-30);
			mover = new PlayerMover(animatorController, movementCollider);

			CompleteAttack dashTowardComplete = new CompleteAttack(dashToward, dashTowardsAnim);
			CompleteAttack dashBackComplete = new CompleteAttack(dashBack, dashBackAnim);

			ThumbStickDashInterpreter thumbstickDashInterpreter = new ThumbStickDashInterpreter(dashTowardComplete, dashBackComplete, 6);

			DebugControls.rectangles.Add(new RenderableRectangleCollider(movementCollider, new Color(Color.Blue, .5f)));

			List<RectangleCollider> hurtboxes = new List<RectangleCollider>();
			hurtboxes.Add(new RectangleCollider(0, 0, 40, 85));
			PlayerHitResolver hitResolver = new PlayerHitResolver(hurtboxes);

			StunPlayer stunPlayer = new StunPlayer();

			Dictionary<string, CompleteAttack> neutralAttacks = new Dictionary<string, CompleteAttack>();
			Dictionary<string, CompleteAttack> crouchAttacks = new Dictionary<string, CompleteAttack>();
			Dictionary<string, CompleteAttack> jumpAttacks = new Dictionary<string, CompleteAttack>();

			CompleteAttack sp1Complete = new CompleteAttack(CharacterAnimation.SPECIALONE, sp1Attack, 2, sp1Anim);
			CompleteAttack sp2Complete = new CompleteAttack(CharacterAnimation.SPECIALTWO, sp2Attack, 2, sp2Anim);
			sp1Complete.Projectile = projectile;

			neutralAttacks.Add(CharacterAnimation.LIGHT, new CompleteAttack(lightAttack, LightAnim));
			neutralAttacks.Add(CharacterAnimation.HEAVY, new CompleteAttack(axeKickAttack, axeKickAnim));
			neutralAttacks.Add(CharacterAnimation.SPECIALONE, sp1Complete);
			neutralAttacks.Add(CharacterAnimation.SPECIALTWO, sp2Complete);

			crouchAttacks.Add(CharacterAnimation.LIGHT, new CompleteAttack(cLightAttack, cLightAnim));
			crouchAttacks.Add(CharacterAnimation.HEAVY, new CompleteAttack(cHeavyAttack, cHeavyAnim));

			jumpAttacks.Add(CharacterAnimation.LIGHT, new CompleteAttack(jLightAttack, jLightAnim));
			jumpAttacks.Add(CharacterAnimation.HEAVY, new CompleteAttack(jHeavyAttack, jHeavyAnim));
			jumpAttacks.Add(CharacterAnimation.SPECIALONE, new CompleteAttack(sp1AirAttack, sp1AirAnim));

			Dictionary<string, Node<CompleteAttack>> stringStarterAttacks = new Dictionary<string, Node<CompleteAttack>>();
			Node<CompleteAttack> LightStarter = new Node<CompleteAttack>(new CompleteAttack(CharacterAnimation.LIGHT, null,1, null));
			Node<CompleteAttack> cLightStarter = new Node<CompleteAttack>(new CompleteAttack(CharacterAnimation.LIGHT, null, 1, null));
			Node<CompleteAttack> heavyStarter = new Node<CompleteAttack>(new CompleteAttack(CharacterAnimation.HEAVY, null, 1, null));
			Node<CompleteAttack> LightExtention1 = new Node<CompleteAttack>(new CompleteAttack(CharacterAnimation.LIGHT, LightString2Attack, 1, lightString2Anim));
			Node<CompleteAttack> LightExtention2 = new Node<CompleteAttack>(new CompleteAttack(CharacterAnimation.LIGHT, LightString3Attack, 1, lightString3Anim));
			Node<CompleteAttack> heavyExtention2 = new Node<CompleteAttack>(new CompleteAttack(CharacterAnimation.HEAVY, heavyAttack, 1, heavyAnim));
			Node<CompleteAttack> sp1 = new Node<CompleteAttack>(sp1Complete);
			Node<CompleteAttack> sp2 = new Node<CompleteAttack>(sp2Complete);

			LightStarter.Add(LightExtention1);
			LightStarter.Add(sp1);
			LightStarter.Add(sp2);
			LightExtention1.Add(LightExtention2);
			LightExtention1.Add(sp1);
			LightExtention1.Add(sp2);
			LightExtention2.Add(sp1);
			LightExtention2.Add(sp2);

			cLightStarter.Add(sp1);
			cLightStarter.Add(sp2);

			heavyStarter.Add(heavyExtention2);
			heavyStarter.Add(sp2);
			heavyStarter.Add(sp1);
			heavyExtention2.Add(sp1);
			heavyExtention2.Add(sp2);

			stringStarterAttacks.Add("STANDLIGHT", LightStarter);
			stringStarterAttacks.Add("CROUCHLIGHT", cLightStarter);
			stringStarterAttacks.Add("STANDHEAVY", heavyStarter);

			AttackQueue attackQueue = new AttackQueue();
			InputQueue inputQueue = new InputQueue(stringStarterAttacks);
			CompleteAttackPlayer completeAttackPlayer = new CompleteAttackPlayer();

			StringStarterAttackResolver neutralAttackResolver = new StringStarterAttackResolver(completeAttackPlayer, inputQueue, neutralAttacks, "STAND");
			StringStarterAttackResolver crouchingAttackResolver = new StringStarterAttackResolver(completeAttackPlayer, inputQueue, crouchAttacks, "CROUCH");
			StringBufferAttackResolver attackingAttackResolver = new StringBufferAttackResolver(inputQueue);
			GenericAttackResolver jumpingAttackResolver = new GenericAttackResolver(completeAttackPlayer, jumpAttacks);

			Dictionary<FighterState, IAttackResolver> attackResolvers = new Dictionary<FighterState, IAttackResolver>();
			attackResolvers.Add(FighterState.neutral, neutralAttackResolver);
			attackResolvers.Add(FighterState.crouch, crouchingAttackResolver);
			attackResolvers.Add(FighterState.jumping, jumpingAttackResolver);
			attackResolvers.Add(FighterState.attackStartup, attackingAttackResolver);
			AttackResolver attackResolver = new AttackResolver(state, attackResolvers);

			VirutalButtonsContainer buttonsContainer = new VirutalButtonsContainer();

			RepeatingVirtualButton lightButton = new RepeatingVirtualButton(CharacterAnimation.LIGHT, attackResolver.PlayAttack,6);
			RepeatingVirtualButton heavyButton = new RepeatingVirtualButton(CharacterAnimation.HEAVY, attackResolver.PlayAttack, 6);
			RepeatingVirtualButton specialOneButton = new RepeatingVirtualButton(CharacterAnimation.SPECIALONE, attackResolver.PlayAttack, 6);
			RepeatingVirtualButton specialTwoButton = new RepeatingVirtualButton(CharacterAnimation.SPECIALTWO, attackResolver.PlayAttack, 6);
			moves.Add(Buttons.X, lightButton);
			moves.Add(Buttons.Y, heavyButton);
			moves.Add(Buttons.A, specialOneButton);
			moves.Add(Buttons.B, specialTwoButton);

			buttonsContainer.AddButton(lightButton);
			buttonsContainer.AddButton(heavyButton);
			buttonsContainer.AddButton(specialOneButton);
			buttonsContainer.AddButton(specialTwoButton);

			Dictionary<String, SoundEffect> sounds = new Dictionary<string, SoundEffect>();
			sounds.Add(CharacterSound.SLAP_HIT, Content.Load<SoundEffect>("sound/slapHit"));
			sounds.Add(CharacterSound.HEAVY_HIT, Content.Load<SoundEffect>("sound/HeavyHit"));
			sounds.Add(CharacterSound.BLOCK, Content.Load<SoundEffect>("sound/block"));
			sounds.Add(CharacterSound.HIT_GROUND, Content.Load<SoundEffect>("sound/hitGround"));

			DeathPlayer deathPlayer = new DeathPlayer(deathAttack);
			PlayerSounds playerSounds = new PlayerSounds(sounds);

			input.SetThumbstick(mover.CheckForInput);

			morrigan.addComponent(input);
			morrigan.addComponent(inputQueue);
			morrigan.addComponent(attackQueue);
			morrigan.addComponent(jumpPlayer);
			morrigan.addComponent(spriteAnimator);
			morrigan.addComponent(mover);
			morrigan.addComponent(attackPlayer);
			morrigan.addComponent(state);
			morrigan.addComponent(stunPlayer);
			morrigan.addComponent(health);
			morrigan.addComponent(spriteRenderer);
			morrigan.addComponent(hitResolver); 
			morrigan.addComponent(airRecoveryPlayer);
			morrigan.addComponent(deathPlayer);
			morrigan.addComponent(playerSounds);
			morrigan.addComponent(completeAttackPlayer);
			morrigan.addComponent(pushbackPlayer);
			morrigan.addComponent(buttonsContainer);
			morrigan.addComponent(thumbstickDashInterpreter);





			morrigan.Load(Content);
			morrigan.transform.Position = new Vector2(0, 0);

			spriteAnimator.PlayAnimation(CharacterAnimation.NEUTRAL);
			scene.addEntity(projectile);
			scene.addEntity(airProjectile);

			PlayerSetter.SetPlayer(playerNumber, morrigan);

			return morrigan;
		}

		public static Entity LoadProjectile(int playernumber, ContentManager Content) {
			Entity projectile = new Entity();

			Hitbox hitbox = new Hitbox(80, 30, 30, 30, new Vector2(-10, 0), new RectangleCollider(0, 0, 15, 15), new Vector2(0, 0), CancelState.light, General.Enums.HitSpark.light, ignorePushback: true);
			Projectile projectileComponent = new Projectile(hitbox, playernumber);
			GenericMover mover = new GenericMover(new Vector2(7, 0));
			TimedDisable timedDisable = new TimedDisable();

			SpriteRenderer spriteRenderer = new SpriteRenderer();
			spriteRenderer.Offset = new Vector2(20, 30);

			SingleSpriteAnimator ssAnimator = new SingleSpriteAnimator(spriteRenderer);
			LoopingSpriteAnimator loopingAnimator = new LoopingSpriteAnimator(spriteRenderer);

			SpriteAnimation activeAnim = new SpriteAnimation(AnimationType.Loop);
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-0")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-1")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-2")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-3")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-4")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-5")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-6")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-7")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-8")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/active/m_10800-9")), 1));

			SpriteAnimation disipateAnim = new SpriteAnimation(AnimationType.Single);
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-0")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-1")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-2")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-3")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-4")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-5")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-6")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-7")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-8")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-9")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-10")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-11")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/projectile/disipate/m_10801-12")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(null, 1));

			Dictionary<String, SpriteAnimation> animations = new Dictionary<String, SpriteAnimation>();
			animations.Add("ACTIVE", activeAnim);
			animations.Add("DISIPATE", disipateAnim);

			Dictionary<AnimationType, ISpriteAnimator> animators = new Dictionary<AnimationType, ISpriteAnimator>();
			animators.Add(AnimationType.Single, ssAnimator);
			animators.Add(AnimationType.Loop, loopingAnimator);

			SpriteAnimator animator = new SpriteAnimator(animations, animators);

			projectile.addComponent(timedDisable);
			projectile.addComponent(mover);
			projectile.addComponent(animator);
			projectile.addComponent(spriteRenderer);
			projectile.addComponent(projectileComponent);
			projectile.Load(Content);
			projectile.enabled = false;
			return projectile;
		}

		public static Entity LoadAirProjectile(int playernumber, ContentManager Content) {
			Entity projectile = new Entity();

			Hitbox hitbox = new Hitbox(80, 30, 30, 30, new Vector2(-3, 0), new RectangleCollider(0, 0, 15, 15), new Vector2(0, 0), CancelState.light, General.Enums.HitSpark.light, ignorePushback: true);
			Projectile projectileComponent = new Projectile(hitbox, playernumber);
			GenericMover mover = new GenericMover(new Vector2(3, -3));
			TimedDisable timedDisable = new TimedDisable();

			SpriteRenderer spriteRenderer = new SpriteRenderer();
			spriteRenderer.Offset = new Vector2(20, 30);

			SingleSpriteAnimator ssAnimator = new SingleSpriteAnimator(spriteRenderer);
			LoopingSpriteAnimator loopingAnimator = new LoopingSpriteAnimator(spriteRenderer);

			SpriteAnimation activeAnim = new SpriteAnimation(AnimationType.Loop);
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-0")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-1")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-2")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-3")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-4")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-5")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-6")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-7")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-8")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-9")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-10")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-11")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-12")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-13")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-14")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-15")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-16")), 1));
			activeAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/m_10820-17")), 1));

			SpriteAnimation disipateAnim = new SpriteAnimation(AnimationType.Single);
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-0")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-1")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-2")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-3")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-4")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-5")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-6")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-7")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-8")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-9")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-10")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-11")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-12")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(new Sprite(Content.Load<Texture2D>("characters/morrigan/airProjectile/disipate/m_10821-13")), 1));
			disipateAnim.AddFrame(new SpriteAnimationFrame(null, 1));

			Dictionary<String, SpriteAnimation> animations = new Dictionary<String, SpriteAnimation>();
			animations.Add("ACTIVE", activeAnim);
			animations.Add("DISIPATE", disipateAnim);

			Dictionary<AnimationType, ISpriteAnimator> animators = new Dictionary<AnimationType, ISpriteAnimator>();
			animators.Add(AnimationType.Single, ssAnimator);
			animators.Add(AnimationType.Loop, loopingAnimator);

			SpriteAnimator animator = new SpriteAnimator(animations, animators);


			projectile.addComponent(timedDisable);
			projectile.addComponent(mover);
			projectile.addComponent(animator);
			projectile.addComponent(spriteRenderer);
			projectile.addComponent(projectileComponent);
			projectile.Load(Content);
			projectile.enabled = false;
			return projectile;
		}
		static Attack BuildJump(int x, AttackPlayer attackPlayer) {
			Attack jump = new Attack(34);
			ActionFrame jumpFrame = new ActionFrame(2);
			jumpFrame.setMovement(new Vector2(x, 9));
			jump.AddActionFrame(jumpFrame, 6);
			jumpFrame = new ActionFrame(8);
			jumpFrame.setMovement(new Vector2(x, 6));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(10);
			jumpFrame.setMovement(new Vector2(x, 4));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(12);
			jumpFrame.setMovement(new Vector2(x, 2));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(14);
			jumpFrame.setMovement(new Vector2(x, 1));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(16);
			jumpFrame.setMovement(new Vector2(x, 0));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(18);
			jumpFrame.setMovement(new Vector2(x, -1));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(20);
			jumpFrame.setMovement(new Vector2(x, -2));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(22);
			jumpFrame.setMovement(new Vector2(x, -4));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(24);
			jumpFrame.setMovement(new Vector2(x, -6));
			jump.AddActionFrame(jumpFrame, 2);
			jumpFrame = new ActionFrame(26);
			jumpFrame.setMovement(new Vector2(x, -9));
			jump.AddActionFrame(jumpFrame, 6);

			jumpFrame = new ActionFrame(32);
			jumpFrame.SetOptionalFunction(attackPlayer.CancelAttacks);
			jump.AddActionFrame(jumpFrame, 1);

			return jump;

		}
	}

}
