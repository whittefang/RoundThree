﻿using EngineFang;
using EngineFang.Animation;
using EngineFang.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RoundThree.EngineFang;
using RoundThree.Level;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree {
	class VanityLevel {

		public static Entity Load(ContentManager content, Scene scene) {

			Entity level = new Entity();

			List<Entity> layer4Entities = new List<Entity>();
			LevelLayer layer4 = new LevelLayer(.5f, layer4Entities);

			List<Entity> layer3Entities = new List<Entity>();
			LevelLayer layer3 = new LevelLayer(.4f, layer3Entities);

			List<Entity> layer2Entities = new List<Entity>();
			LevelLayer layer2 = new LevelLayer(.1f, layer2Entities);

			List<Entity> layer1Entities = new List<Entity>();
			LevelLayer layer1 = new LevelLayer(0f, layer1Entities);


			// layer four
			SpriteRenderer renderer = new SpriteRenderer();
			renderer.Sprite = new Sprite(content.Load<Texture2D>("levels/vanity/vanity_0-3"));
			Entity background = new Entity();
			background.addComponent(renderer);
			layer4Entities.Add(background);
			scene.addEntity(background);


			// layer three
			background = new Entity();
			renderer = new SpriteRenderer();
			renderer.Sprite = new Sprite(content.Load<Texture2D>("levels/vanity/vanity_0-2"));
			background.addComponent(renderer);
			layer3Entities.Add(background);
			scene.addEntity(background);

			background = new Entity();
			renderer = new SpriteRenderer();
			background.addComponent(renderer);
			LoopingSpriteAnimatorComponent animator = new LoopingSpriteAnimatorComponent(renderer);
			SpriteAnimation animation = new SpriteAnimation(AnimationType.Loop);
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_7-0")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_7-1")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_7-2")), 10));
			background.addComponent(animator);
			animator.PlayAnimation(animation);
			layer3Entities.Add(background);
			scene.addEntity(background);

			// layer 2
			background = new Entity();
			renderer = new SpriteRenderer();
			renderer.Sprite = new Sprite(content.Load<Texture2D>("levels/vanity/vanity_0-1"));
			background.addComponent(renderer);
			layer2Entities.Add(background);
			scene.addEntity(background);

			background = new Entity();
			renderer = new SpriteRenderer();
			renderer.Sprite = new Sprite(content.Load<Texture2D>("levels/vanity/vanity_5-0"));
			background.addComponent(renderer);
			layer2Entities.Add(background);
			scene.addEntity(background);

			background = new Entity();
			renderer = new SpriteRenderer();
			background.addComponent(renderer);
			animator = new LoopingSpriteAnimatorComponent(renderer);
			animation = new SpriteAnimation(AnimationType.Loop);
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-0")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-1")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-2")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-3")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-4")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-5")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-6")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-7")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-8")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_1-9")), 10));
			background.addComponent(animator);
			animator.PlayAnimation(animation);
			layer2Entities.Add(background);
			scene.addEntity(background);

			background = new Entity();
			renderer = new SpriteRenderer();
			background.addComponent(renderer);
			animator = new LoopingSpriteAnimatorComponent(renderer);
			animation = new SpriteAnimation(AnimationType.Loop);
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_2-0")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_2-1")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_2-2")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_2-3")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_2-4")), 10));
			background.addComponent(animator);
			animator.PlayAnimation(animation);
			layer2Entities.Add(background);
			scene.addEntity(background);

			background = new Entity();
			renderer = new SpriteRenderer();
			background.addComponent(renderer);
			animator = new LoopingSpriteAnimatorComponent(renderer);
			animation = new SpriteAnimation(AnimationType.Loop);
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_3-0")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_3-1")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_3-2")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_3-3")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_3-4")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_3-5")), 10));
			background.addComponent(animator);
			animator.PlayAnimation(animation);
			layer2Entities.Add(background);
			scene.addEntity(background);

			background = new Entity();
			renderer = new SpriteRenderer();
			background.addComponent(renderer);
			animator = new LoopingSpriteAnimatorComponent(renderer);
			animation = new SpriteAnimation(AnimationType.Loop);
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_4-0")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_4-1")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_4-2")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_4-3")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_4-4")), 10));
			background.addComponent(animator);
			animator.PlayAnimation(animation);
			layer2Entities.Add(background);
			scene.addEntity(background);

			background = new Entity();
			renderer = new SpriteRenderer();
			background.addComponent(renderer);
			animator = new LoopingSpriteAnimatorComponent(renderer);
			animation = new SpriteAnimation(AnimationType.Loop);
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-1")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-2")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-3")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-4")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-5")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-6")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-7")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-8")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-9")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-10")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-11")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_6-12")), 10));
			background.addComponent(animator);
			animator.PlayAnimation(animation);
			layer2Entities.Add(background);
			scene.addEntity(background);

			background = new Entity();
			renderer = new SpriteRenderer();
			background.addComponent(renderer);
			animator = new LoopingSpriteAnimatorComponent(renderer);
			animation = new SpriteAnimation(AnimationType.Loop);
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_8-0")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_8-1")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_8-2")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_8-3")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_8-4")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_8-5")), 10));
			animation.AddFrame(new SpriteAnimationFrame(new Sprite(content.Load<Texture2D>("levels/vanity/vanity_8-6")), 10));
			background.addComponent(animator);
			animator.PlayAnimation(animation);
			layer2Entities.Add(background);
			scene.addEntity(background);


			//layer  one
			background = new Entity();
			renderer = new SpriteRenderer();
			renderer.Sprite = new Sprite(content.Load<Texture2D>("levels/vanity/vanity_0-0"));
			background.addComponent(renderer);
			layer1Entities.Add(background);
			scene.addEntity(background);

			

			



			List<LevelLayer> layers = new List<LevelLayer>();
			layers.Add(layer1);
			layers.Add(layer2);
			layers.Add(layer3);
			layers.Add(layer4);
			LevelBackground levelBackground = new LevelBackground(layers);

			level.addComponent(levelBackground);


			return level;
		}
	}
}
