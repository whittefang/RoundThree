﻿using EngineFang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;

namespace RoundThree.General {
	class Health: Component {
		private float maxHealth;
		private float whiteDamage;
		private float currentHealth;
		private PlayerUI healthBar;
		private DeathPlayer deathPlayer;

		public Health(float maxHealth, PlayerUI healthBar) {
			this.maxHealth = maxHealth;
			currentHealth = maxHealth;
			this.healthBar = healthBar;
			whiteDamage = 0;
		}

		public override void Load(ContentManager content) {
			deathPlayer = entity.getComponent<DeathPlayer>();
		}

		public void DealDamage(float damage) {
			currentHealth -= damage;
			whiteDamage = 0;
			healthBar.UpdateHealth(currentHealth/maxHealth);
			healthBar.UpdateWhiteHealth((currentHealth + whiteDamage) / maxHealth);
			healthBar.AddComboDamage(damage);
			if (currentHealth <= 0) {
				deathPlayer.PlayDeath();
				RoundHandler.EndRound(entity.getComponent<PlayerState>().PlayerNumber);
			}

		}

		public void ResetHealth() {
			currentHealth = maxHealth;
			healthBar.UpdateHealth(currentHealth / maxHealth);
		}
		public void DealWhiteDamage(float damage) {
			whiteDamage += damage;
			currentHealth -= damage;
			healthBar.UpdateHealth(currentHealth / maxHealth);
			healthBar.UpdateWhiteHealth((currentHealth + whiteDamage) / maxHealth);
		}
	}
}
