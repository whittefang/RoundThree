﻿using EngineFang;
using Microsoft.Xna.Framework;
using RoundThree.General.Attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using EngineFang.Input;

namespace RoundThree.General {
	class ThumbStickDashInterpreter : Component, Updatable {
		const float XDEAD_ZONE = .25f;
		const float YDEAD_ZONE = .5f;
		Vector2 previousInput;
		Queue<InputEvent> commandHistory;
		CompleteAttack towardDash, backDash;
		CompleteAttackPlayer completeAttackPlayer;
		PlayerState state;
		Input input;
		int dashBuffer;
		int remainingDashBuffer;
		int currentDashDirection;

		public ThumbStickDashInterpreter(CompleteAttack towardDash, CompleteAttack backDash, int dashBuffer) {
			this.backDash = backDash;
			this.towardDash = towardDash;
			this.dashBuffer = dashBuffer;

			commandHistory = new Queue<InputEvent>(15);
			for (int i = 0; i < 15; i++) {
				commandHistory.Enqueue(InputEvent.Neutral);
			}
		}

		public override void Load(ContentManager content) {
			completeAttackPlayer = entity.getComponent<CompleteAttackPlayer>();
			state = entity.getComponent<PlayerState>();
			input = entity.getComponent<Input>();
		}

		// return 0 for no dash
		// 1 for right
		// 2 for left
		public int Interpret(Vector2 thumbstickInput) {
			commandHistory.Dequeue();
			InputEvent currentEvent = getCurrentEvent(thumbstickInput);
			
			if (checkForValidInput(currentEvent)) {
				if (currentEvent == InputEvent.LeftPress) {
					currentDashDirection = -1;
					remainingDashBuffer = dashBuffer;
				} else if (currentEvent == InputEvent.RightPress) {
					currentDashDirection = 1;
					remainingDashBuffer = dashBuffer;
				}
			}

			commandHistory.Enqueue(currentEvent);
			previousInput = thumbstickInput;
			return currentDashDirection;
		}

		private bool checkForValidInput(InputEvent currentEvent) {
			foreach (var input in commandHistory.Reverse()) {
				if (input == currentEvent) {
					return true;
				} else if (input == InputEvent.Neutral) {
					continue;
				} else { break; }
			}
			return false;
		}

		private InputEvent getCurrentEvent(Vector2 thumbstickInput) {
			InputEvent currentEvent;
			if (thumbstickInput.X > XDEAD_ZONE && previousInput.X < XDEAD_ZONE) {
				currentEvent = InputEvent.RightPress;
			} else if (thumbstickInput.X < -XDEAD_ZONE && previousInput.X > -XDEAD_ZONE) {
				currentEvent = InputEvent.LeftPress;
			} else if (Math.Abs(thumbstickInput.Y) > YDEAD_ZONE) {
				currentEvent = InputEvent.UpDown;
			} else {
				currentEvent = InputEvent.Neutral;
			}
			return currentEvent;
		}

		public void Update() {
			// we need to always record the input so that a buffer is useful
			if (state.CurrentState == FighterState.neutral || state.CurrentState == FighterState.crouch) {
				int dashDirection = Interpret(input.GetLeftThumbStick());
				if (remainingDashBuffer > 0) {
					remainingDashBuffer--;
					dashDirection = currentDashDirection;
					Dash(dashDirection);
				} 
			}
		}

		private void Dash(int dash) {
			if ((state.IsFacingRight && dash == 1) || (!state.IsFacingRight && dash == -1)) {
				completeAttackPlayer.PlayCompleteAttack(towardDash);
				remainingDashBuffer = 0;
			} else if ((state.IsFacingRight && dash == -1) || (!state.IsFacingRight && dash == 1)) {
				completeAttackPlayer.PlayCompleteAttack(backDash);
				remainingDashBuffer = 0;
			}
		}

		enum InputEvent {
			LeftPress,
			RightPress,
			Neutral,
			UpDown
		}
	}
}
