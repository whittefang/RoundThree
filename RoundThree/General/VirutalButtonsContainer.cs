﻿using EngineFang;
using EngineFang.Input;
using RoundThree.General.Attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class VirutalButtonsContainer : Component, Updatable {
		List<Updatable> buttons;

		public VirutalButtonsContainer() {
			buttons = new List<Updatable>();
		}

		public void AddButton(Updatable button) {
			buttons.Add(button);
		}

		public void Update() {
			foreach (Updatable button in buttons) {
				button.Update();
			}
		}
	}
}
