﻿using EngineFang;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class PlayerSounds: Component {
		Dictionary<String,SoundEffect> sounds;

		public PlayerSounds(Dictionary<string, SoundEffect> sounds) {
			this.sounds = sounds;
		}

		public void PlaySound(String sound) {
			sounds[sound].Play();
		}
	}
}
