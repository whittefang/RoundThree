﻿using EngineFang;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class ObjectFollower : Component, Updatable {
		Entity entityToFollow;

		public ObjectFollower(Entity entityToFollow) {
			this.entityToFollow = entityToFollow;
		}

		public void Update() {
			transform.Position = new Vector2(entityToFollow.transform.Position.X, transform.Position.Y);
		}
	}
}
