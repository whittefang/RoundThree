﻿using EngineFang;
using EngineFang.Animation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using RoundThree.EngineFang;
using RoundThree.General.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	static class HitsparksPool {
		static ObjectPool objectPool;
		static Dictionary<HitSpark, SpriteAnimation> animations;


		public static void SetObjectPool(ObjectPool objectPool) {
			HitsparksPool.objectPool = objectPool;
		}
		public static void SetAnimations(Dictionary<HitSpark, SpriteAnimation> animations) {
			HitsparksPool.animations = animations;
		}

		public static void PlaySpark(HitSpark strength, Vector2 position) {

			// get hitspark
			Entity spark = objectPool.Get();
			// place it at position
			spark.transform.Position = position;
			// play its animation
			SpriteAnimation animation = animations[strength];
			spark.getComponent<SingleSpriteAnimatorComponent>().PlayAnimation(animation);
			spark.getComponent<TimedDisable>().StartTimedDisable();

		}


	}
}
