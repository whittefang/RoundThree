﻿using EngineFang;
using EngineFang.ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class PlayerUI : Component, Updatable {
		private UIBar healthBar;
		private UIBar whiteHealthBar;
		private UIBar superBar;
		private UIText comboCounterText;
		private UIText comboDamageText;

		private int comboCounter;
		private float comboDamage;
		private int comboTextFadeCounter;

		public PlayerUI(UIBar healthBar, UIBar whiteHealthBar, UIBar superBar, UIText comboCounterText, UIText comboDamageText) {
			this.healthBar = healthBar;
			this.whiteHealthBar = whiteHealthBar;
			this.superBar = superBar;
			this.comboCounterText = comboCounterText;
			this.comboDamageText = comboDamageText;
		}

		public void UpdateHealth(float amount) {
			healthBar.ChangeFillAmount(amount);
		}
		public void UpdateWhiteHealth(float amount) {
			whiteHealthBar.ChangeFillAmount(amount);
		}
		public void UpdateSuper(float amount) {
			superBar.ChangeFillAmount(amount);
		}

		public void AddComboDamage(float damage) {
			comboDamage += damage;
			comboCounter++;
			comboDamageText.Text = comboDamage.ToString();
			comboCounterText.Text = comboCounter.ToString();
			comboDamageText.Enabled = true;
			comboCounterText.Enabled = true;
			comboTextFadeCounter = 30;
		}

		public void Update() {
			if (comboTextFadeCounter > 0) {
				comboTextFadeCounter--;
				if (comboTextFadeCounter <= 0) {
					comboDamageText.Enabled = false;
					comboCounterText.Enabled = false;

					comboCounter = 0;
					comboDamage = 0;
				}
			}
		}
	}
}
