﻿using EngineFang;
using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using RoundThree.General.Enums;

namespace RoundThree.General {
	class AirRecoveryPlayer : Component, Updatable {
		private bool recovering;
		private PlayerMover mover;
		private SpriteAnimator animator;
		private PlayerState state;
		private Vector2 moveAmount;

		public override void Load(ContentManager content) {
			mover = entity.getComponent<PlayerMover>();
			animator = entity.getComponent<SpriteAnimator>();
			state = entity.getComponent<PlayerState>();
			moveAmount = new Vector2(0, -5);
		}

		public void Recover() {
			recovering = true;
			state.CurrentState = FighterState.attackRecovery;
			animator.PlayAnimation(CharacterAnimation.AIRRECOVERY);
		}

		public void AirHitRecover() {
			recovering = true;
		}

		public void Update() {
			if (recovering) {
				mover.MoveTowards(moveAmount);
				if (!IsInAir()) {
					transform.Position = new Vector2( transform.Position.X,LevelInformation.FloorPosition);
					recovering = false;
					if (state.CurrentState == FighterState.attackRecovery) {
						state.CurrentState = FighterState.neutral;
					}
				}
			}
		}

		public bool IsInAir() {
			return transform.Position.Y > LevelInformation.FloorPosition;
		}
	}
}
