﻿using EngineFang.Animation;
using RoundThree.General.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General
{
    class CharacterAnimationController
    {
        //TODO: extract to a character state machine
        private bool facingRight = true;
        SpriteAnimator animator;
        PlayerState state;

        public CharacterAnimationController(SpriteAnimator animator, PlayerState state) {
            // placeholder to avoid null on first run
            this.animator = animator;
            this.state = state;
        }

        public void PlayWalk(float x) {
                if (((x > 0 && state.IsFacingRight) || (x < 0 && !state.IsFacingRight)) && !CharacterAnimation.WALKTOWARD.Equals(animator.CurrentAnimation))
                {
                    animator.PlayAnimation(CharacterAnimation.WALKTOWARD);
                }
                else if (((x < 0 && state.IsFacingRight) || (x > 0 && !state.IsFacingRight)) && !CharacterAnimation.WALKBACK.Equals(animator.CurrentAnimation))
                {
                    animator.PlayAnimation(CharacterAnimation.WALKBACK);
                }
        }
        public void PlayNeutral()
        {
            if (!CharacterAnimation.NEUTRAL.Equals(animator.CurrentAnimation) && state.CurrentState == FighterState.neutral)
            {
                animator.PlayAnimation(CharacterAnimation.NEUTRAL);
            }
        }
        public void PlayCrouch()
        {
            if (!CharacterAnimation.CROUCH.Equals(animator.CurrentAnimation) && state.CurrentState == FighterState.neutral)
            {
                animator.PlayAnimation(CharacterAnimation.CROUCH);
            }
        }
        public void Play(String anim) {
            animator.PlayAnimation(anim);
        }
    }
}
