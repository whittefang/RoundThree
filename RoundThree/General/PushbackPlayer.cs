﻿using EngineFang;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class PushbackPlayer : Component, Updatable {

		PlayerMover mover;
		int hitstunMovementRemaining;
		Vector2 hitstunMovementAmount;

		public override void Load(ContentManager content) {

			mover = entity.getComponent<PlayerMover>();
		}
		public void StartPushback(int duration, Vector2 amount) {
			hitstunMovementAmount = amount;
			hitstunMovementRemaining = duration;
		}
		public void StopPushback() {
			hitstunMovementRemaining = 0;
		}
		public void Update() {
			if (hitstunMovementRemaining > 0) {
				mover.MoveTowards(hitstunMovementAmount);
				hitstunMovementRemaining--;
			}
		}
	}
}
