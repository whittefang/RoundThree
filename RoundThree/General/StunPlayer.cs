﻿using EngineFang;
using EngineFang.Animation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using RoundThree.General.Attacks;
using RoundThree.General.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class StunPlayer : Component, Updatable {
		int remainingStun, remainingKnockdown;
		bool isStunned, isKnockedDown;
		PlayerState state;
		SpriteAnimator animator;
		AirRecoveryPlayer airRecoveryPlayer;
		PushbackPlayer pushbackPlayer;

		public override void Load(ContentManager content) {
			state = entity.getComponent<PlayerState>();
			animator = entity.getComponent<SpriteAnimator>();
			airRecoveryPlayer = entity.getComponent<AirRecoveryPlayer>();
			pushbackPlayer = entity.getComponent<PushbackPlayer>();
		}

		public void StartHitStun(Hitbox hitbox) {
			if (state.CurrentState == FighterState.jumping
				|| state.CurrentState == FighterState.jumpingAttack
				|| state.CurrentState == FighterState.airHitstun
				|| transform.Position.Y > LevelInformation.FloorPosition
				|| hitbox.attackProperty == AttackProperty.Launcher) {
				StartAirborneHitstun();
			}  else {
				StartGroundedHitstun(hitbox.hitstun);
			}
			isStunned = true;
			pushbackPlayer.StartPushback(hitbox.pushbackDuration, hitbox.pushback);
		}

		private void StartGroundedHitstun(int hitstun) {
			animator.PlayAnimation(CharacterAnimation.HIT);
			state.CurrentState = FighterState.hitstun;
			remainingStun = hitstun;
		}
		private void StartAirborneHitstun() {
			animator.PlayAnimation(CharacterAnimation.AIRHIT);
			state.CurrentState = FighterState.airHitstun;
			airRecoveryPlayer.AirHitRecover();
			remainingStun = 6;
		}


		public void StartBlockStun(Hitbox hitbox) {
			animator.PlayAnimation(CharacterAnimation.BLOCK);
			state.CurrentState = FighterState.blockstun;
			isStunned = true;
			remainingStun = hitbox.blockstun;
			pushbackPlayer.StartPushback(hitbox.pushbackDuration, new Vector2( hitbox.blockPushback, 0));
		}

		public void CancelStun() {
			isStunned = false;
			remainingStun = 0;
		}

		public void Update() {
			if (isStunned) {
				UpdateStun();
			}
			if (isKnockedDown) {
				UpdateKnockdownRecovery();
			}
		}

		void UpdateStun() {
			remainingStun--;
			if (remainingStun <= 0) {

				if (state.CurrentState == FighterState.hitstun || state.CurrentState == FighterState.blockstun) {
					isStunned = false;
					state.CurrentState = FighterState.neutral;
				} else if (airRecoveryPlayer.IsInAir()) {
					airRecoveryPlayer.AirHitRecover();
				} else  {
					// TODO: seperate the knockdown from the getting up
					animator.PlayAnimation(CharacterAnimation.KNOCKDOWN);
					isStunned = false;
					isKnockedDown = true;
					remainingKnockdown = 30;
					state.CurrentState = FighterState.invincible;
				}
			}
			

		}
		void UpdateKnockdownRecovery() {
			remainingKnockdown--;
			if (remainingKnockdown <= 0) {
				isKnockedDown = false;
				state.CurrentState = FighterState.neutral;
			}
		}
	}
}
