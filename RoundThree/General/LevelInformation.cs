﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class LevelInformation {

		public static float FloorPosition {get; set; }
		public static float Width { get; set; }
	}
}
