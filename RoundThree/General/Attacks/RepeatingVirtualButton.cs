﻿using EngineFang;
using EngineFang.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	class RepeatingVirtualButton: VirtualButton, Updatable{
		String attackIdentifier;
		public delegate bool pressButton(String attackIdentifier);
		pressButton button;
		int repeats;
		int repeatsLeft;

		public RepeatingVirtualButton(String attackIdentifier, pressButton button, int repeats) {
			this.attackIdentifier = attackIdentifier;
			this.button = button;
			this.repeats = repeats;
		}

		public void PressButton() {
			repeatsLeft = repeats;
			TryPress();
		}

		public void Update() {
			if (repeatsLeft > 0) {
				repeatsLeft--;
				TryPress();
			}
		}

		void TryPress() {
			if (button(attackIdentifier)) {
				repeatsLeft = 0;
			}
		}
	}
}
