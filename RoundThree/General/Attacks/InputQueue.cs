﻿using EngineFang;
using RoundThree.EngineFang.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;

namespace RoundThree.General.Attacks {
	class InputQueue: Component {
		private Dictionary<String, Node<CompleteAttack>> attackStarters;
		private Node<CompleteAttack> currentNode;
		private AttackQueue attackQueue;


		public InputQueue(Dictionary<string, Node<CompleteAttack>> attackStarters) {
			this.attackStarters = attackStarters;
		}

		public override void Load(ContentManager content) {
			attackQueue = entity.getComponent<AttackQueue>();
		}

		public void StartString(String starterAttack) {
			attackStarters.TryGetValue(starterAttack, out currentNode);
		}

		public void AddButtonToQue(String button) {
			if (currentNode != null) {
				CompleteAttack attack = getNextButton(button);

				if (attack != null) {
					attackQueue.AddAttackToQueue(attack);
				} 
			}
		}

		private CompleteAttack getNextButton(String button) {
			foreach (Node<CompleteAttack> node in currentNode.GetChildren()) {
				if (node.GetData().Id == button) {
					currentNode = node;
					return node.GetData();
				}
			}
			return null;
		}

		public void ResetQueue() {
			currentNode = null;
		}
	}
}
