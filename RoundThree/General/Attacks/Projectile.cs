﻿using EngineFang;
using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using EngineFang.Graphics;
using Microsoft.Xna.Framework.Graphics;

namespace RoundThree.General.Attacks {
	class Projectile : Component, Updatable, Renderable {
		GenericMover mover;
		SpriteAnimator spriteAnimator;
		SpriteRenderer renderer;
		TimedDisable timedDisable;
		Hitbox hitbox;
		int playerNumber;
		int OFF_SCREEN_BUFFER = 70;
		bool active = false;


		public Projectile(Hitbox hitbox, int playerNumber) {
			this.hitbox = hitbox;
			this.playerNumber = playerNumber;
		}

		public override void Load(ContentManager content) {
			mover = entity.getComponent<GenericMover>();
			spriteAnimator = entity.getComponent<SpriteAnimator>();
			renderer = entity.getComponent<SpriteRenderer>();
			timedDisable = entity.getComponent<TimedDisable>();
		}

		public void Activate(bool moveRight) {
			if (moveRight) {
				mover.Direction = new Vector2(1, 1);
			} else {
				mover.Direction = new Vector2(-1, 1);
			}
			renderer.Flip(!moveRight);
			active = true;
			mover.Active = true;
			hitbox.RefreshID();
			spriteAnimator.PlayAnimation("ACTIVE");
			entity.enabled = true;
		}

		public void Update() {
			if (active) {
				hitbox.setPosition(transform.Position, 0);
				if (transform.Position.X > Camera.GetBound() + OFF_SCREEN_BUFFER || transform.Position.X < Camera.GetBound(false) - OFF_SCREEN_BUFFER) {
					Deactivate();
				}
				if (HitResolver.CheckForHit(hitbox, playerNumber) || HitResolver.CheckForProjectileClash(hitbox, playerNumber)) {
					Deactivate();
				}
			}
		}

		public void Deactivate() {

			spriteAnimator.PlayAnimation("DISIPATE");
			active = false;
			mover.Active = false;
			timedDisable.StartTimedDisable(15);
		}

		public bool IsActive() {
			return active;
		}

		public Hitbox GetHitbox() {
			return hitbox;
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (DebugControls.debugModeEnabled && active) {
				spriteBatch.Draw(DebugControls.debugSquareTexture, hitbox.hitboxBounds.GetRectangle(), new Color(Color.Red, .5f));
			}
		}
	}
}
