﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using RoundThree.General.Enums;
using RoundThree.EngineFang;

namespace RoundThree.General.Attacks {
	class Hitbox {
		static int currentID = 0;

		public Hitbox(int damage, int chipDamage, int hitstun, int blockstun, Vector2 pushback,
			RectangleCollider hitboxBounds, Vector2 positionOffset, CancelState cancelStrength,
			HitSpark attackStrength, AttackProperty attackProperty = AttackProperty.Hit, int pushbackDuration = 5,
			int hitStop = -1, bool ignorePushback = false, ThrowType throwType = ThrowType.none, float blockPushback = -999) {
			this.damage = damage;
			this.chipDamage = chipDamage;
			this.hitstun = hitstun;
			this.blockstun = blockstun;
			this.pushbackDuration = pushbackDuration;
			this.pushback = pushback;
			this.blockPushback = blockPushback;
			this.hitboxBounds = hitboxBounds;
			this.positionOffset = positionOffset;
			this.attackProperty = attackProperty;
			this.cancelStrength = cancelStrength;
			this.attackStrength = attackStrength;
			this.ignorePushback = ignorePushback;
			this.throwType = throwType;
			//moveMasterID = MasterObjectContainer.GetMoveMasterID();
			if (blockPushback == -999) {
				this.blockPushback = pushback.X;
			}

			id = Hitbox.GetUniqueID();
			if (hitStop == -1) {
				if (attackStrength == HitSpark.light) {
					this.hitStop = 2;
					soundKey = CharacterSound.SLAP_HIT;

				} else if (attackStrength == HitSpark.medium) {
					this.hitStop = 4;
					soundKey = CharacterSound.SLAP_HIT;
				} else if (attackStrength == HitSpark.heavy) {
					this.hitStop = 6;
					soundKey = CharacterSound.HEAVY_HIT;
				} else {
					this.hitStop = 8;
					soundKey = CharacterSound.SLAP_HIT;
				}
			} else {
				this.hitStop = hitStop;
			}
		}


		public int damage;
		public int chipDamage;
		public int hitstun;
		public int blockstun;
		public int hitStop;
		public int pushbackDuration;
		//public int blockStop; // should this exist?
		public Vector2 pushback;
		public float blockPushback;
		public RectangleCollider hitboxBounds;
		public Vector2 positionOffset;
		public CancelState cancelStrength;
		public HitSpark attackStrength;
		public AttackProperty attackProperty;
		public int id;
		public bool ignorePushback;
		public ThrowType throwType;
		public string soundKey;

		public void setPosition(Vector2 position, float direction) {

			Vector2 finalPosition = position;
			// add in offset from original transform and change direction
			finalPosition += new Vector2(positionOffset.X * direction, positionOffset.Y);
			hitboxBounds.SetPosition(finalPosition);
		}
		public void RefreshID() {
			id = GetUniqueID();
		}
		static public int GetUniqueID() {
			currentID++;
			return currentID;
		}
	}
}
