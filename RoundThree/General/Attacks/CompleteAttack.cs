﻿using EngineFang;
using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	class CompleteAttack {
		public Attack Attack { get; }
		public string Id { get; }
		public int CancelStrength { get; }
		public SpriteAnimation Animation { get; }
		public Entity Projectile { get; set; }

		public CompleteAttack(Attack attack, SpriteAnimation animation) : this("null", attack, 0,animation) { }

		public CompleteAttack(string id, Attack attack, int CancelStrength, SpriteAnimation animation) {
			this.Attack = attack;
			this.Id = id;
			this.Animation = animation;
			this.CancelStrength = CancelStrength;
		}
	}
}
