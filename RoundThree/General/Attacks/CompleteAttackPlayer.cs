﻿using EngineFang;
using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;

namespace RoundThree.General.Attacks {
	class CompleteAttackPlayer: Component{
		private AttackPlayer attackPlayer;
		private SpriteAnimator spriteAnimator;

		public override void Load(ContentManager content) {
			attackPlayer = entity.getComponent<AttackPlayer>();
			spriteAnimator = entity.getComponent<SpriteAnimator>();
		}

		public void PlayCompleteAttack(CompleteAttack completeAttack) {
			if (completeAttack.Projectile != null && completeAttack.Projectile.enabled) {
				return;
			}

			attackPlayer.StartAttack(completeAttack.Attack);
			spriteAnimator.PlayAnimation(completeAttack.Animation);
		}

	}
}
