﻿using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	class StringBufferAttackResolver: IAttackResolver {

		InputQueue inputQueue;

		public StringBufferAttackResolver(InputQueue inputQueue) {
			this.inputQueue = inputQueue;
		}

		public bool StartAttack(string attackIdentifier) {
			inputQueue.AddButtonToQue(attackIdentifier);
			return true;
		}
	}
}
