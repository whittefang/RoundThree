﻿using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	class StringStarterAttackResolver: IAttackResolver {
		CompleteAttackPlayer completeAttackPlayer;
		InputQueue inputQueue;
		String stateIdentifier;
		Dictionary<String, CompleteAttack> fighterActions;

		public StringStarterAttackResolver(CompleteAttackPlayer completeAttackPlayer, InputQueue inputQueue,Dictionary<String, CompleteAttack> fighterActions, string stateIdentifier) {
			this.completeAttackPlayer = completeAttackPlayer;
			this.fighterActions = fighterActions;
			this.inputQueue = inputQueue;
			this.stateIdentifier = stateIdentifier;
		}

		public bool StartAttack(string attackIdentifier) {
			CompleteAttack foundCompleteAttack;
			if (fighterActions.TryGetValue(attackIdentifier, out foundCompleteAttack)) {
				completeAttackPlayer.PlayCompleteAttack(foundCompleteAttack);
				inputQueue.StartString(stateIdentifier + attackIdentifier);
				return true;
			}
			return false;
		}
	}
}
