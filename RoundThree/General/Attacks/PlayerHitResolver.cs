﻿using EngineFang;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using RoundThree.EngineFang;
using RoundThree.General.Enums;
using Microsoft.Xna.Framework.Content;
using EngineFang.Animation;

namespace RoundThree.General.Attacks {
	class PlayerHitResolver : Component, Updatable,Renderable {
		public List<RectangleCollider> Hurtboxes { get; set; }
		PlayerState state;
		StunPlayer stunPlayer;
		Health health;
		AttackPlayer attackPlayer;
		SpriteAnimator animator;
		JumpPlayer jumpPlayer;
		InputQueue inputQueue;
		AttackQueue attackQueue;
		PlayerSounds sound;
		int lastMoveID;


		public PlayerHitResolver(List<RectangleCollider> hurtboxes) {
			this.Hurtboxes = hurtboxes;
		}
		public override void Load(ContentManager content) {
			state = entity.getComponent<PlayerState>();
			stunPlayer = entity.getComponent<StunPlayer>();
			health = entity.getComponent<Health>();
			animator = entity.getComponent<SpriteAnimator>();
			attackPlayer = entity.getComponent<AttackPlayer>();
			jumpPlayer = entity.getComponent<JumpPlayer>();
			inputQueue = entity.getComponent<InputQueue>();
			attackQueue = entity.getComponent<AttackQueue>();
			sound = entity.getComponent<PlayerSounds>();
		}


		public bool ProcessHit(Hitbox hitbox, Vector2 hitpoint) {
			if (lastMoveID != hitbox.id) {
				lastMoveID = hitbox.id;
				// check if invincible
				if (state.CurrentState == FighterState.invincible) {
					return false;
				} else if ((state.IsBlocking() || state.CurrentState == FighterState.blockstun) && hitbox.attackProperty != AttackProperty.Throw) {
					// block attack
					CancelActions();
					state.ResolveFacingDirection();
					HitstopController.PlayHitstop(hitbox.hitStop / 2);
					HitsparksPool.PlaySpark(HitSpark.block, hitpoint);
					sound.PlaySound(CharacterSound.BLOCK);
					stunPlayer.StartBlockStun(hitbox);
					health.DealWhiteDamage(hitbox.chipDamage);
					return true;
				} else if (hitbox.attackProperty == AttackProperty.Throw && state.CurrentState != FighterState.hitstun
						&& state.CurrentState != FighterState.blockstun && state.CurrentState != FighterState.jumping
						&& state.CurrentState != FighterState.jumpingAttack && state.CurrentState != FighterState.airHitstun) {
					// sucessful throw
					return true;
				} else {
					// sucessful hit
					CancelActions();
					state.ResolveFacingDirection();
					HitstopController.PlayHitstop(hitbox.hitStop);
					HitsparksPool.PlaySpark(hitbox.attackStrength, hitpoint);
					sound.PlaySound(hitbox.soundKey);
					stunPlayer.StartHitStun(hitbox);
					health.DealDamage(hitbox.damage);
					return true;
				}
			}
			return false;
		}

		public void CancelActions() {
			attackPlayer.CancelAttacks();
			animator.StopAnimation();
			jumpPlayer.cancelJump();
			inputQueue.ResetQueue();
			attackQueue.CancelAttacks();
			stunPlayer.CancelStun();
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (DebugControls.debugModeEnabled) {
				foreach (RectangleCollider rect in Hurtboxes) {
					spriteBatch.Draw(DebugControls.debugSquareTexture, rect.GetRectangle(), new Color(Color.Green, .5f));
				}
			}
		}

		public void Update() {
			foreach (RectangleCollider rect in Hurtboxes) {
				rect.SetPosition(transform.Position);
			}

		}
	}
}
