﻿using EngineFang;
using EngineFang.Animation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace RoundThree.General.Attacks {
	class AttackPlayer : Component, Updatable, Renderable {

		PlayerState state;
		bool isAttacking;
		bool attackActive = false;
		Vector2 attackDirection;
		PlayerMover playerMover;
		Attack currentAttack;
		AttackQueue attackQueue;
		int currentAttackIndex = 0;
		int currentCancelStrength = 0;

		public AttackPlayer() {
			attackDirection = new Vector2(1, 0);
		}

		public override void Load(ContentManager content) {
			state = entity.getComponent<PlayerState>();
			playerMover = entity.getComponent<PlayerMover>();
			attackQueue = entity.getComponent<AttackQueue>();
		}

		// handle beginning of attack command
		public void StartAttack(Attack newAttack) {
			currentAttack = newAttack;
			isAttacking = true;
			//update to use action frames state in the future
			state.CurrentState = newAttack.StartingState;
			state.CancelStrength = 0;
			currentAttackIndex = 0;
			currentCancelStrength = 0;

			// start the current attack
		}

		public void CancelAttacks() {
			//stop current attack
			isAttacking = false;
			attackActive = false;
		}

		// handle attack updates
		void ProcessAttacking() {
			if (isAttacking) {
				ActionFrame currentActionFrame = currentAttack.getActionFrame(currentAttackIndex);
				if (currentActionFrame != null) {
					// move 
					if (currentActionFrame.isMovement) {
						playerMover.MoveTowards(currentActionFrame.movementTranslation);
					}

					if (currentActionFrame.isCancelFrame) {
						state.CancelStrength = currentCancelStrength+1 ;
					} else {
						state.CancelStrength = 0;
					}

					// activate hitbox
					if (currentActionFrame.isAttack) {
						ActivateHitbox(currentActionFrame);
					} else {
						attackActive = false;
					}
					// run optional function
					if (currentActionFrame.optionalFunction != null) {
						currentActionFrame.optionalFunction();
					}

					// run optional sound effect if it exists
					if (currentActionFrame.optionalSound != null) {
						currentActionFrame.optionalSound.Play();
					}




				} else {
					attackActive = false;
					state.CancelStrength = 0;
				}
				if (currentAttackIndex >= currentAttack.Length) {
					FinishAttack();
				}
				currentAttackIndex++;
			}
		}

		public void ActivateHitbox(ActionFrame currentActionFrame) {

			attackActive = true;
			// check if this is the first frame of an attack and mark its id 
			// else set the following active frame ids to the same as the previous
			if (currentAttack.getActionFrame(currentAttackIndex - 1) == null || !currentAttack.getActionFrame(currentAttackIndex - 1).isAttack) {
				currentActionFrame.hitbox.RefreshID();
			} 

			currentActionFrame.hitbox.setPosition(transform.Position, attackDirection.X);
			// check for hit

			bool successfulHit = HitResolver.CheckForHit(currentActionFrame.hitbox, state.PlayerNumber);
			if (successfulHit) {
				currentCancelStrength++;
			}
		}

		public void FinishAttack() {

			isAttacking = false;
			attackQueue.CancelAttacks();
			currentCancelStrength = 0;
			if (state.CurrentState == FighterState.attackStartup) {
				state.CurrentState = FighterState.neutral;
			}


		}
		public void SetAttackDirection(bool faceRight) {
			int direction;
			if (faceRight) {
				direction = 1;
			} else {
				direction = -1;
			}
			attackDirection.X = direction;
		}
		public float GetAttackDirection() {
			return attackDirection.X;
		}
		public void Update() {
			ProcessAttacking();
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (DebugControls.debugModeEnabled && attackActive) {
				spriteBatch.Draw(DebugControls.debugSquareTexture, currentAttack.getActionFrame(currentAttackIndex - 1).hitbox.hitboxBounds.GetRectangle(), new Color(Color.Red, .5f));
			}
		}

	}
}
