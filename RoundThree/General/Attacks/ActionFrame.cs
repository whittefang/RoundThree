﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
    class ActionFrame
    {
        public int activeFrame;
        public bool isAttack;
        public bool isMovement;
		public bool isCancelFrame;
        public Vector2 movementTranslation;
        public Hitbox hitbox;
        public delegate void voidDel();
        public SoundEffect optionalSound;
        public voidDel optionalFunction, optionalHitFunction;
        //TODO: add variable character state options

        public ActionFrame(int activeFrame)
        {
            this.activeFrame = activeFrame -1;
        }

        public ActionFrame(ActionFrame actionframe)
        {
            activeFrame = actionframe.activeFrame;
            isAttack = actionframe.isAttack;
            isMovement = actionframe.isMovement;
            movementTranslation = actionframe.movementTranslation;
            hitbox = actionframe.hitbox;
            optionalFunction = actionframe.optionalFunction;
            optionalHitFunction = actionframe.optionalHitFunction;
            optionalSound = actionframe.optionalSound;
        }

        public void setAttack(Hitbox hitbox)
        {
            this.hitbox = hitbox;
            isAttack = true;
        }
        public void setMovement(Vector2 offset)
        {
            movementTranslation = offset;
            isMovement = true;
        }
        public void setActiveFrame(int activeFrame)
        {
            this.activeFrame = activeFrame;
        }
		public void SetOptionalFunction(voidDel optionalFunction) {
			this.optionalFunction = optionalFunction;
		}
		public void setOptionalSound(SoundEffect newSound)
        {
            optionalSound = newSound;
        }
		public void SetCancelFrame() {
			isCancelFrame = true;
		}

        

    }
}
