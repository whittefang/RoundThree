﻿using EngineFang.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	class SingleFireAttackVirtualButton : VirtualButton {
		String attackIdentifier;
		public delegate void pressButton(String attackIdentifier);
		pressButton button;
		public SingleFireAttackVirtualButton(String attackIdentifier, pressButton button) {
			this.attackIdentifier = attackIdentifier;
			this.button = button;
		}

		public void PressButton() {
			button(attackIdentifier);
		}
	}
}
