﻿using EngineFang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	class Attack {


		Dictionary<int, ActionFrame> actionFrames;
		int length;



		public Attack( int length, FighterState startingState)
        {
            this.actionFrames = new Dictionary<int, ActionFrame>();
            this.length = length;
			StartingState = startingState;
        }
		public Attack(int length) : this(length, FighterState.attackStartup){}

		public int Length
        {
            get{return length;}
        }
		public FighterState StartingState { get;}


		public ActionFrame getActionFrame(int index) {
           
			ActionFrame foundFrame;
			actionFrames.TryGetValue(index, out foundFrame);
			return foundFrame;
        }


        public void AddActionFrame(ActionFrame actionFrame, int duration = 1)
        {

            int initialActiveFrame = actionFrame.activeFrame;

            for (int i = 0; i < duration; i++)
            {
                // if an action frame exists we replace relevant fields
                // WARING: possible to overwrite important data 
                if (actionFrames.ContainsKey(initialActiveFrame + i))
                {
                    if (actionFrame.isAttack)
                    {
                        actionFrames[initialActiveFrame + i].setAttack(actionFrame.hitbox);
                    }
                    if (actionFrame.isMovement)
                    {
                        actionFrames[initialActiveFrame + i].setMovement(actionFrame.movementTranslation);
                    }
                    if (actionFrame.optionalFunction != null)
                    {
                        actionFrames[initialActiveFrame + i].optionalFunction = actionFrame.optionalFunction;
                    }
                    if (actionFrame.optionalHitFunction != null)
                    {
                        actionFrames[initialActiveFrame + i].optionalHitFunction = actionFrame.optionalHitFunction;
                    }
					if (actionFrame.isCancelFrame) {
						actionFrames[initialActiveFrame + i].SetCancelFrame();
					}
                    if (actionFrame.optionalSound != null)
                    {
                        actionFrames[initialActiveFrame + i].optionalSound = actionFrame.optionalSound;
                    }
                }
                else {
                    actionFrames.Add(initialActiveFrame + i, actionFrame);
                    actionFrame = new ActionFrame(actionFrame);
                    actionFrame.setActiveFrame(initialActiveFrame + i);
                }
            }
        }


    }
}
