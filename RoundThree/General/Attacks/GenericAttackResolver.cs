﻿using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	class GenericAttackResolver : IAttackResolver {
		CompleteAttackPlayer completeAttackPlayer;

		Dictionary<String, CompleteAttack> fighterActions;

		public GenericAttackResolver(CompleteAttackPlayer completeAttackPlayer, Dictionary<String, CompleteAttack> fighterActions) {
			this.completeAttackPlayer = completeAttackPlayer;
			this.fighterActions = fighterActions;
		}

		public bool StartAttack(string attackIdentifier) {
			CompleteAttack foundCompleteAttack;
			if (fighterActions.TryGetValue(attackIdentifier, out foundCompleteAttack)) {
				completeAttackPlayer.PlayCompleteAttack(foundCompleteAttack);
				return true;
			}
			return false;
		}
	}
}
