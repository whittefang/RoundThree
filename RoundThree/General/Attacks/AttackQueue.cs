﻿using EngineFang;
using EngineFang.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;

namespace RoundThree.General.Attacks {
	class AttackQueue : Component, Updatable {
		private CompleteAttackPlayer completeAttackPlayer;

		private PlayerState state;
		private Queue<CompleteAttack> attacks;

		public AttackQueue() {
			attacks = new Queue<CompleteAttack>();
		}

		public override void Load(ContentManager content) {
			completeAttackPlayer = entity.getComponent<CompleteAttackPlayer>();
			state = entity.getComponent<PlayerState>();
		}

		public void Update() {
			ProccessQueue();
		}

		public void CancelAttacks() {
			attacks.Clear();
		}

		public void AddAttackToQueue(CompleteAttack completeAttack) {
			attacks.Enqueue(completeAttack);
		}

		private void ProccessQueue() {
			if (attacks.Count > 0) {
				CompleteAttack currentAttack = attacks.Peek();
				int cancelStrength = currentAttack.CancelStrength;
				if (state.CancelStrength >= cancelStrength) {
					completeAttackPlayer.PlayCompleteAttack(attacks.Dequeue());
				}
			}
		}
	}
}
