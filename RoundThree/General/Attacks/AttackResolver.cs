﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	class AttackResolver {

		PlayerState state;
		Dictionary<FighterState, IAttackResolver> attackResolvers;

		public AttackResolver(PlayerState state, Dictionary<FighterState, IAttackResolver> attackResolvers) {
			this.state = state;
			this.attackResolvers = attackResolvers;
		}

		public bool PlayAttack(String attackIdentifier) {
			IAttackResolver foundResolver;
			if (attackResolvers.TryGetValue(state.CurrentState, out foundResolver)){
				return foundResolver.StartAttack(attackIdentifier);
			}
			return false;
		}

	}
}
