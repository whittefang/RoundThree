﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Attacks {
	interface IAttackResolver {
		bool StartAttack(String attackIdentifier);
	}
}
