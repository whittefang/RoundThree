﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Enums {
	class CharacterSound {
		public static String SLAP_HIT = "slaphit";
		public static String HEAVY_HIT = "heavyhit";
		public static String BLOCK = "block";
		public static String HIT_GROUND = "hitground";
	}
}
