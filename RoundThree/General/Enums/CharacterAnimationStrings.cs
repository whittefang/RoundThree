﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Enums {
	class CharacterAnimation {
		public static readonly String NEUTRAL = "NEUTRAL";
		public static readonly String WALKTOWARD = "WALKTOWARD";
		public static readonly String WALKBACK = "WALKBACK";

		public static readonly String JUMPNEUTRAL = "JUMPNEUTRAL";
		public static readonly String JUMPTOWARD = "JUMPTOWARD";
		public static readonly String JUMPAWAY = "JUMPAWAY";

		public static readonly String AIRRECOVERY = "AIRRECOVERY";

		public static readonly String DASHTOWARD = "DASHTOWARD";
		public static readonly String DASHBACK = "DASHBACK";

		public static readonly String LIGHT = "LIGHT";
		public static readonly String MEDIUM = "MEDIUM";
		public static readonly String HEAVY = "HEAVY";

		public static readonly String SPECIALONE = "SPECIALONE";
		public static readonly String SPECIALTWO = "SPECIALTWO";
		public static readonly String SPECIALTHREE = "SPECIALTHREE";

		public static readonly String HIT = "HIT";
		public static readonly String BLOCK = "BLOCK";

		public static readonly String INTRO = "INTRO";
		public static readonly String DEATH = "DEATH";
		public static readonly String WIN = "WIN";

		public static readonly String AIRHIT = "AIRHIT";
		public static readonly String KNOCKDOWN = "KNOCKDOWN";
		public static readonly String SUPER = "SUPER";
		public static readonly String CROUCH = "CROUCH";
		public static readonly String THROW = "THROW";
	}
}
