﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General
{
    enum FighterState
    {
        neutral,
		crouch,
        walk,
        jumping,
        invincible,
        hitstun,
        airHitstun,
        blockstun,
        projectileInvincible,
        attackStartup,
        attackRecovery,
        jumpingAttack
    }
}
