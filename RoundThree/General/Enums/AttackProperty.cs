﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General.Enums
{
    enum AttackProperty
    {
        Throw,
        Launcher,
        Kockdown,
        Hit
    }
}
