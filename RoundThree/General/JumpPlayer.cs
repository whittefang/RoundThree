﻿using EngineFang;
using RoundThree.General.Attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using EngineFang.Animation;
using RoundThree.General.Enums;

namespace RoundThree.General {
	class JumpPlayer : Component, Updatable {

		Attack jumpNeutral;
		Attack jumpToward;
		Attack jumpBack;

		Attack currentJump;
		int currentJumpIndex;

		PlayerState state;
		PlayerMover playerMover;
		CharacterAnimationController animator;

		bool isJumping = false;

		public JumpPlayer(CharacterAnimationController animator,Attack jumpNeutral, Attack jumpToward, Attack jumpAway) {
			this.jumpNeutral = jumpNeutral;
			this.jumpToward = jumpToward;
			this.jumpBack = jumpAway;
			this.animator = animator;
		}

		public override void Load(ContentManager content) {
			state = entity.getComponent<PlayerState>();
			playerMover = entity.getComponent<PlayerMover>();
		}

		public void Jump(float x) {
			isJumping = true;
			currentJumpIndex = 0;

			if ((x > 0 && state.IsFacingRight) || (x < 0 && !state.IsFacingRight)) {
				currentJump = jumpToward;
				state.CurrentState = FighterState.jumping;
				animator.Play(CharacterAnimation.JUMPNEUTRAL);
			} else if ((x < 0 && state.IsFacingRight) || (x > 0 && !state.IsFacingRight)) {
				currentJump = jumpBack;
				state.CurrentState = FighterState.jumping;
				animator.Play(CharacterAnimation.JUMPNEUTRAL);
			} else {
				currentJump = jumpNeutral;
				state.CurrentState = FighterState.jumping;
				animator.Play(CharacterAnimation.JUMPNEUTRAL);
			}
		}

		public void Update() {
			if (isJumping) {
				ActionFrame currentActionFrame = currentJump.getActionFrame(currentJumpIndex);
				if (currentActionFrame != null) {
					// move 
					if (currentActionFrame.isMovement) {
						playerMover.MoveTowards(currentActionFrame.movementTranslation);
					}

					// run optional sound effect if it exists
					if (currentActionFrame.optionalSound != null) {
						currentActionFrame.optionalSound.Play();
					}
					if (currentActionFrame.optionalFunction != null) {
						currentActionFrame.optionalFunction();
					}

				} 
				if (currentJumpIndex >= currentJump.Length) {
					cancelJump();
					state.CurrentState = FighterState.neutral;
				}
				currentJumpIndex++;
			}

		}

		public void cancelJump() {
			isJumping = false;
		}

	}
}
