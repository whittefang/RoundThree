﻿using EngineFang;
using Microsoft.Xna.Framework;
using RoundThree.EngineFang;
using RoundThree.General;
using RoundThree.General.Attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	static class HitResolver {

		static List<PlayerHitResolver> playerHitResolvers;
		static List<PushbackPlayer> pushbackPlayers;

		static List<List<Projectile>> projectiles;
		static Vector2 hitpoint;

		static HitResolver() {
			playerHitResolvers = new List<PlayerHitResolver>();
			projectiles = new List<List<Projectile>>();
			pushbackPlayers = new List<PushbackPlayer>();
			projectiles.Add(new List<Projectile>());
			projectiles.Add(new List<Projectile>());
		}

		public static void SetPlayerHitResolver(int playerNumber, PlayerHitResolver hitResolver) {
			playerHitResolvers.Insert(playerNumber, hitResolver);
			pushbackPlayers.Insert(playerNumber, hitResolver.entity.getComponent<PushbackPlayer>());
		}
		public static void SetProjectile(int playerNumber, Projectile projectile) {
			projectiles[playerNumber].Add(projectile);
		}
		public static bool CheckForHit(Hitbox attackersHitbox, int playerNumber) {
			int hitPlayersNumber = 0;
			if (playerNumber == 0) {
				hitPlayersNumber = 1;
			}

			List<RectangleCollider> hurtboxes = playerHitResolvers[hitPlayersNumber].Hurtboxes;

			if (IsIntersecting(hurtboxes, attackersHitbox)) {

				

				bool result = playerHitResolvers[hitPlayersNumber].ProcessHit(attackersHitbox, hitpoint);
				float hitPlayerXPos = playerHitResolvers[hitPlayersNumber].transform.Position.X;


				if (result && (hitPlayerXPos == Camera.GetBound() || hitPlayerXPos == Camera.GetBound(false))) {
					// TODO: make it so that this plays over time
					if (!attackersHitbox.ignorePushback) {
						//TODO: make this play block or hitstun based on situation
						pushbackPlayers[playerNumber].StartPushback(attackersHitbox.pushbackDuration,new Vector2(attackersHitbox.pushback.X, 0));
					}
				}
				return result;
			}

			return false;
		}

		private static bool IsIntersecting(List<RectangleCollider> hurtboxes, Hitbox hitbox) {
			foreach (RectangleCollider hurtbox in hurtboxes) {
				if (hitbox.hitboxBounds.GetRectangle().Intersects(hurtbox.GetRectangle())) {
					Rectangle hitUnion = Rectangle.Intersect(hitbox.hitboxBounds.GetRectangle(), hurtbox.GetRectangle());
					hitpoint = new Vector2(hitUnion.X + hitUnion.Width / 2, -(hitUnion.Y - hitUnion.Height / 2));
					return true;
				}
			}
			return false;
		}

		public static bool CheckForProjectileClash(Hitbox hitbox, int playerNumber) {
			int otherPlayerNumber = 0;
			if (playerNumber == 0) {
				otherPlayerNumber = 1;
			}
			List<Projectile> enemyProjectiles = projectiles[otherPlayerNumber];

			foreach (Projectile projectile in enemyProjectiles) {
				if (projectile.IsActive()) {
					if (projectile.GetHitbox().hitboxBounds.GetRectangle().Intersects(hitbox.hitboxBounds.GetRectangle())) {
						projectile.Deactivate();
						return true;
					}
				}
			}
			return false;
		}
	}
}
