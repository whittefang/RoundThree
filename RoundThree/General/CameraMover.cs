﻿using EngineFang;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class CameraMover: Component, Updatable {
		private const int DEFAULT_HEIGHT = 57;
		private const int CAMERA_MOVE_BUFFER = 60;
		private const int CAMERA_MOVE_SPEED = 5;
		private float PLAYER_DISTANCE_BUFFER;
		Entity[] players;

		float[] playerPrevPos;

		public CameraMover(Entity playerOne, Entity playerTwo) {
			players = new Entity[2];
			playerPrevPos = new float[2];
			players[0] = playerOne;
			players[1] = playerTwo;
			PLAYER_DISTANCE_BUFFER = (Camera.GetBound() - CAMERA_MOVE_BUFFER)*2;
		}

		public void Update() {
			

			// if players are > than a certain distance apart put camera in center?
			float distanceApart = Math.Abs(players[0].transform.Position.X - players[1].transform.Position.X);
			if (distanceApart > PLAYER_DISTANCE_BUFFER) {
				float newPos = (players[0].transform.Position.X + players[1].transform.Position.X) / 2;

				transform.Position = new Vector2(newPos, DEFAULT_HEIGHT);
			} else {
				// else check if player is pushing a boundry
				float translation = 0;

				for (int i = 0; i < 2; i++) {
					float direction = players[i].transform.Position.X - playerPrevPos[i];
					translation += MoveCamera(players[i], direction);
				}

				for (int i = 0; i < 2; i++) {
					translation = CorrectForPlayer(players[i], translation);
				}
				transform.Position = new Vector2(transform.Position.X + translation, DEFAULT_HEIGHT);
			}

			if (transform.Position.X > LevelInformation.Width) {
				transform.Position = new Vector2(LevelInformation.Width, DEFAULT_HEIGHT);
			}else if(transform.Position.X < -LevelInformation.Width) {
				transform.Position = new Vector2(-LevelInformation.Width, DEFAULT_HEIGHT);
			}

			Camera.SetPosition(transform.Position);

			for (int i = 0; i < 2; i++) {
				playerPrevPos[i] = players[i].transform.Position.X;
			}
		}

		public float MoveCamera(Entity player, float direction) {
			float rightBound = Camera.GetBound() - CAMERA_MOVE_BUFFER;
			float leftBound = Camera.GetBound(false) + CAMERA_MOVE_BUFFER;
			float translation = 0;
			if (direction > 0 && player.transform.Position.X > rightBound ) {
				// move right
				translation = direction;

			}else if (direction < 0 && player.transform.Position.X < leftBound ) {
				// move left
				translation = direction;

			}
			return translation;
		}

		public float CorrectForPlayer(Entity Player, float translation) {
			if ((translation > 0 && Player.transform.Position.X < Camera.GetBound(false) + translation)
				|| (translation < 0 && Player.transform.Position.X > Camera.GetBound() + translation)) {
				return 0;
			}
			return translation;
		}
		public void ResetCamera() {
			float newPos = (players[0].transform.Position.X + players[1].transform.Position.X) / 2;
			transform.Position = new Vector2(newPos, DEFAULT_HEIGHT);
			Camera.SetPosition(transform.Position);
		}
	}
}
