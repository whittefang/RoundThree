﻿using EngineFang;
using EngineFang.Graphics;
using EngineFang.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using RoundThree.General.Attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class PlayerState : Component, Updatable {
		FighterState currentState;
		AttackPlayer attackPlayer;
		SpriteRenderer renderer;
		Input input;
		PlayerIndex playerIndex;
		public int PlayerNumber { get; set; }
		public int CancelStrength { get; set; }
		bool playerOne;
		bool isFacingRight;
		CancelState cancelState;

		internal FighterState CurrentState {
			get { return currentState; }
			set { currentState = value; }
		}

		public bool PlayerOne {
			get { return playerOne; }
		}

		public bool IsFacingRight {
			get { return isFacingRight; }
		}



		public PlayerState(int index) {
			if (index == 1) {
				playerOne = false;
			} else {
				playerOne = true;
			}
			playerIndex = (PlayerIndex)index;
			PlayerNumber = index;
		}

		public override void Load(ContentManager content) {

			input = entity.getComponent<Input>();
			renderer = entity.getComponent<SpriteRenderer>();
			attackPlayer = entity.getComponent<AttackPlayer>();

			if (PlayerOne) {
				isFacingRight = true;
				renderer.Flip(false);
				attackPlayer.SetAttackDirection(true);
			} else {
				isFacingRight = false;
				renderer.Flip(true);
				attackPlayer.SetAttackDirection(false);
			}
		}

		public bool IsBlocking() {
			if (currentState == FighterState.neutral || currentState == FighterState.crouch) {
				if (PlayerFacingCalculator.IsPlayerFacingRight(playerIndex) && input.GetLeftThumbStick().X < 0) {
					return true;
				} else if (!PlayerFacingCalculator.IsPlayerFacingRight(playerIndex) && input.GetLeftThumbStick().X > 0) {
					return true;
				}
			}
			return false;
		}

		// handle facing direction updates
		public void CalculateFacingDirection() {
			if (CurrentState == FighterState.neutral || CurrentState == FighterState.crouch ) {
				ResolveFacingDirection();
			}
		}
		public void ResolveFacingDirection() {
			if (PlayerFacingCalculator.IsPlayerFacingRight(playerIndex) && !IsFacingRight) {
				isFacingRight = true;
				renderer.Flip(false);
				attackPlayer.SetAttackDirection(true);
			} else if (!PlayerFacingCalculator.IsPlayerFacingRight(playerIndex) && IsFacingRight) {
				isFacingRight = false;
				renderer.Flip(true);
				attackPlayer.SetAttackDirection(false);
			}
		}


		public void Update() {
			CalculateFacingDirection();
		}

	}
}

