﻿using EngineFang;
using EngineFang.Animation;
using EngineFang.Input;
using Microsoft.Xna.Framework;
using RoundThree.General.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	static class RoundHandler {
		static List<Entity> players;
		static int endRoundTimer;
		static int winningPlayerNumber;
		static CameraMover cameraMover;
		static RoundHandler() {
			players = new List<Entity>();
		}

		public static void SetPlayer(int playerNumber, Entity player) {
			players.Insert(playerNumber, player);
		}
		public static void SetCameraMover(CameraMover cameraMover) {
			RoundHandler.cameraMover = cameraMover;
		}

		public static void EndRound(int losingPlayerNumebr) {
			winningPlayerNumber = 0;
			if (losingPlayerNumebr == 0) {
				winningPlayerNumber = 1;
			}

			foreach (Entity player in players) {
				player.getComponent<Input>().Enabled = false;
			}
			endRoundTimer = 180;
			// play win for palyer who won after small delay
			// reset players after delay

		}

		public static void StartRound() {
			// move players to starting position
			// reset health
			// reset meter
			// enable controls 
			// play intro animation
			foreach (Entity player in players) {
				player.getComponent<Input>().Enabled = true;
				player.getComponent<PlayerState>().CurrentState = FighterState.neutral;
				player.getComponent<Health>().ResetHealth();
			}
			players[0].getComponent<PlayerMover>().MoveToPosition( new Vector2(-100, 0));
			players[1].getComponent<PlayerMover>().MoveToPosition(new Vector2(100, 0));
			cameraMover.ResetCamera();
		}
		public static void Update() {
			EndRoundUpdate();
			
		}
		private static void EndRoundUpdate() {
			if (endRoundTimer > 0) {
				endRoundTimer--;
				if (endRoundTimer == 120) {
					// play win 
					players[winningPlayerNumber].getComponent<PlayerState>().CurrentState = FighterState.invincible;
					players[winningPlayerNumber].getComponent<SpriteAnimator>().PlayAnimation(CharacterAnimation.WIN);
				}

				if (endRoundTimer == 1) {
					// reset match
					StartRound();
				}
			}
		}

	}
}
