﻿using EngineFang.Animation;
using RoundThree.General.Attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class FighterAction {


		public Attack Attack { get; }
		public SpriteAnimation Animation { get;}
		

		public FighterAction(Attack attack, SpriteAnimation animation) {
			Attack = attack;
			Animation = animation;
		}
	}
}
