﻿using EngineFang;
using RoundThree.General.Attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using EngineFang.Animation;
using RoundThree.General.Enums;

namespace RoundThree.General {
	class DeathPlayer : Component, Updatable {
		AttackPlayer attackPlayer;
		PlayerState state;
		Attack deathAttack;
		SpriteAnimator animator;
		PlayerHitResolver hitResolver;
		bool waitingToPlayDeath;

		public DeathPlayer(Attack deathAttack) {
			this.deathAttack = deathAttack;
		}

		public override void Load(ContentManager content) {
			attackPlayer = entity.getComponent<AttackPlayer>();
			state = entity.getComponent<PlayerState>();
			hitResolver = entity.getComponent<PlayerHitResolver>();
			animator = entity.getComponent<SpriteAnimator>();
		}

		public void PlayDeath() {
			state.CurrentState = FighterState.invincible;
			if (transform.Position.Y <= LevelInformation.FloorPosition) {
				waitingToPlayDeath = false;
				hitResolver.CancelActions();
				attackPlayer.StartAttack(deathAttack);
				state.CurrentState = FighterState.invincible;
				animator.PlayAnimation(CharacterAnimation.DEATH);
			} else {
				waitingToPlayDeath = true;
			}
		}

		public void Update() {
			if (waitingToPlayDeath) {
				PlayDeath();
			}
		}
	}
}
