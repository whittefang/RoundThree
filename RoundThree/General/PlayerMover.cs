﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using EngineFang;
using EngineFang.Animation;
using Microsoft.Xna.Framework.Content;
using RoundThree.General.Attacks;
using RoundThree.General.Enums;
using RoundThree.EngineFang;

namespace RoundThree.General {
	class PlayerMover : Component {
		const float XAXIS_DEADZONE = .15f;
		const float JUMP_DEADZONE = .35f;
		private float groundSpeed = 5;
		CharacterAnimationController animator;
		PlayerState state;
		AttackPlayer attackPlayer;
		JumpPlayer jumpPlayer;
		RectangleCollider collisionBox;
		public RectangleCollider CollisionBox {
			get { return collisionBox; }
		}

		public PlayerMover(CharacterAnimationController animator, RectangleCollider collisionBox) {
			this.animator = animator;
			this.collisionBox = collisionBox;
		}

		public override void Load(ContentManager content) {
			state = entity.getComponent<PlayerState>();
			attackPlayer = entity.getComponent<AttackPlayer>();
			jumpPlayer = entity.getComponent<JumpPlayer>();
		}

		public float GroundSpeed {
			get { return groundSpeed; }
			set { groundSpeed = value; }
		}

		public void CheckForInput(Vector2 direction) {
			// crouch
			if (direction.Y < -JUMP_DEADZONE && IsLegalMove(direction)) {
				animator.PlayCrouch();
				state.CurrentState = FighterState.crouch;
				return;
			} else if (state.CurrentState.Equals(FighterState.crouch)) {
				state.CurrentState = FighterState.neutral;
			}

			// ground movement
			if (Math.Abs(direction.X) > XAXIS_DEADZONE && IsLegalMove(direction)) {
				Walk(direction);
				animator.PlayWalk(direction.X);
			} else {
				animator.PlayNeutral();
			}

			// jump input
			if (direction.Y > JUMP_DEADZONE && IsLegalMove(direction)) {
				jumpPlayer.Jump(direction.X);
			}
		}

		private bool IsLegalMove(Vector2 direction) {
			return (state.CurrentState == FighterState.neutral || state.CurrentState == FighterState.crouch);
		}

		private void Walk(Vector2 direction) {
			if (direction.X > 0) {
				MovePlayer(new Vector2(groundSpeed, 0));
			} else {
				MovePlayer(new Vector2(-groundSpeed, 0));
			}
		}

		public void MovePlayer(Vector2 translation) {
			MovePlayerWithBoundries(transform.Position + translation);
			//adjust for player collision
			if (PlayerCollisionResolver.ArePlayersColliding()) {
				PlayerCollisionResolver.PushOtherPlayer(state.PlayerNumber);
			}
		}
		public void MovePlayerNoCollision(Vector2 translation) {
			MovePlayerWithBoundries(transform.Position + translation);
		}
		private void MovePlayerWithBoundries(Vector2 position) {

			if (position.X > Camera.GetBound()) {
				position.X = Camera.GetBound();
			} else if (position.X < Camera.GetBound(false)) {
				position.X = Camera.GetBound(false);
			}
			MoveToPosition(position);
		}

		public void MoveToPosition(Vector2 position) {
			transform.Position = position;
			collisionBox.SetPosition(position);
		}

		public void MoveTowards(Vector2 translation) {
			if (state.IsFacingRight) {
				MovePlayer(new Vector2(translation.X, translation.Y));
			} else {
				MovePlayer(new Vector2(-translation.X, translation.Y));
			}
		}

	}
}
