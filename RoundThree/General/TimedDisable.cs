﻿using EngineFang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	class TimedDisable : Component, Updatable {
		int lifetime;
		int currentLifeLeft;

		public TimedDisable(int lifetime) {
			this.lifetime = lifetime;
			currentLifeLeft = lifetime;
		}
		public TimedDisable():this(0) {}

		public void StartTimedDisable() {
			currentLifeLeft = lifetime;
		}
		public void StartTimedDisable(int duration) {
			currentLifeLeft = duration;
		}

		public void Update() {
			if (currentLifeLeft > 0) {
				currentLifeLeft--;
				if (currentLifeLeft <= 0) {
					entity.enabled = false;
				}
			}
		}
	}
}
