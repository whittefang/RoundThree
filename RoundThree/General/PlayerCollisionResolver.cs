﻿using Microsoft.Xna.Framework;
using RoundThree.EngineFang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	static class PlayerCollisionResolver {
		static List<PlayerMover> playerMovers;

		static PlayerCollisionResolver() {
			playerMovers = new List<PlayerMover>();
		}

		public static void SetPlayer(int playerNumber, PlayerMover playerMover) {
			playerMovers.Insert(playerNumber, playerMover);
		}
		public static bool ArePlayersColliding() {
			Rectangle p1Rect = playerMovers[0].CollisionBox.GetRectangle();
			Rectangle p2Rect = playerMovers[1].CollisionBox.GetRectangle();
			return p1Rect.Intersects(p2Rect);
		}
		public static void PushOtherPlayer(int pushingPlayersNumber) {
			int otherPlayerNumber = 0;
			if (pushingPlayersNumber == 0) {
				otherPlayerNumber = 1;
			}

			PlayerMover pushingPlayer = playerMovers[pushingPlayersNumber];
			PlayerMover otherPlayer = playerMovers[otherPlayerNumber];

			Vector2 translation;

			if (pushingPlayer.transform.Position.X > otherPlayer.transform.Position.X) {
				translation = CalculatePushAmount(otherPlayer, pushingPlayer);
				translation.X = -translation.X;
				otherPlayer.MovePlayer(translation);
			} else if (pushingPlayer.transform.Position.X < otherPlayer.transform.Position.X) {
				translation = CalculatePushAmount(pushingPlayer, otherPlayer);
				otherPlayer.MovePlayer(translation);
			} else {
				// player is jumping into the corner in most cases, push them towards the center
				translation = CalculatePushAmount(pushingPlayer, otherPlayer);
				if (pushingPlayer.transform.Position.X < 0) {
					pushingPlayer.MovePlayer(translation);
				} else {
					pushingPlayer.MovePlayer(-translation);
				}
			}
		}

		private static Vector2 CalculatePushAmount(PlayerMover leftPlayer, PlayerMover rightPlayer) {
			Vector2 translation = new Vector2(0,0);
			float leftPlayerBound = leftPlayer.CollisionBox.GetPosition().X + (leftPlayer.CollisionBox.Width/2);
			float rightPlayerBound = rightPlayer.CollisionBox.GetPosition().X - (rightPlayer.CollisionBox.Width / 2);

			translation.X = Math.Abs(leftPlayerBound - rightPlayerBound) +1;

			return translation;
			//return new Vector2(5,0);
		}
	}
}
