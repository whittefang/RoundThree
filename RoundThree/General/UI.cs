﻿using EngineFang;
using EngineFang.Graphics;
using EngineFang.ui;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RoundThree.EngineFang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	static class UI {
		static SpriteFont font;
		static Texture2D healthBarTex;
		static RectangleCollider healthBarLeftRect, healthBarRightRect, superBarRightRect, superBarLeftRect;
		static int healthBarWidth = 167, superMeterWidth = 80;
		static Vector2 healthbarPositionLeft = new Vector2(-31, 193), healthBarPositionRight = new Vector2(31, 193);
		static Vector2 superBarPositionLeft = new Vector2(-81, -15), superBarPositionRight = new Vector2(81, -15);
		static bool DisplayComboDataP1 = false, DisplayComboDataP2 = false;

		static int comboDamageP1, comboDamageP2;
		static int comboCountP1, comboCountP2;
		static int comboFadeBufferP1, comboFadeBufferP2;



		static UI() {

			// bars
			healthBarLeftRect = new RectangleCollider((int)healthbarPositionLeft.X, (int)healthbarPositionLeft.Y , healthBarWidth, 5);
			healthBarRightRect = new RectangleCollider((int)healthBarPositionRight.X, (int)healthBarPositionRight.Y, healthBarWidth, 5);

			superBarLeftRect = new RectangleCollider((int)superBarPositionLeft.X, (int)superBarPositionLeft.Y, superMeterWidth, 4);
			superBarRightRect = new RectangleCollider((int)superBarPositionRight.X, (int)superBarPositionRight.Y, superMeterWidth, 4);

		}

		static public PlayerUI GetNewPlayerUI(Entity  entity, bool playerOne, ContentManager content) {
			int flip = 1;
			if (playerOne) {
				flip = -1;
			}

			UIBar healthBar = new UIBar(content.Load<Texture2D>("square"), new RectangleCollider(-114 * flip, 191, 165, 9), Color.Green);
			UIBar whiteHealthBar = new UIBar(content.Load<Texture2D>("square"), new RectangleCollider(-114 * flip, 191, 165, 9), new Color(200, 200, 200, 150));
			UIBar backgroundBar = new UIBar(content.Load<Texture2D>("square"), new RectangleCollider(-114 * flip, 191, 165, 9), new Color(Color.Red, .75f));
			UIBar superBar = new UIBar(content.Load<Texture2D>("square"), new RectangleCollider(-100 * flip, 0, 175, 5), Color.Blue);
			UIText comboCounterText = new UIText(content.Load<SpriteFont>("arial"), new Vector2(-150 * flip, -150));
			UIText comboDamageText = new UIText(content.Load<SpriteFont>("arial"), new Vector2(-150 * flip, -125));
			PlayerUI playerUI = new PlayerUI(healthBar, whiteHealthBar, superBar, comboCounterText, comboDamageText);


			entity.addComponent(playerUI);
			entity.addComponent(backgroundBar);
			entity.addComponent(whiteHealthBar);
			entity.addComponent(healthBar);
		//	entity.addComponent(superBar);
			entity.addComponent(comboDamageText);
			entity.addComponent(comboCounterText);
			return playerUI;
		}
		static public void LoadUIArt(ContentManager content,Scene scene) {
			Entity leftArt, rightArt;
			leftArt = new Entity();
			rightArt = new Entity();

			SpriteRenderer rendererLeft = new SpriteRenderer(new Sprite(content.Load<Texture2D>("UI/uiLeft")));
			SpriteRenderer rendererRight = new SpriteRenderer(new Sprite(content.Load<Texture2D>("UI/uiRight")));
			leftArt.addComponent(rendererLeft);
			rightArt.addComponent(rendererRight);

			leftArt.transform.Position = new Vector2(-106, 160);
			rightArt.transform.Position = new Vector2(106, 160);

			scene.addEntity(leftArt);
			scene.addEntity(rightArt);
		}

		static public void Load(ContentManager content) {
			healthBarTex = content.Load<Texture2D>("square");
			font = content.Load<SpriteFont>("arial");
		}

		static public void Draw(SpriteBatch spriteBatch) {

			// health bars and super bars
			spriteBatch.Draw(healthBarTex, healthBarLeftRect.GetRectangle(), Color.Green);
			spriteBatch.Draw(healthBarTex, healthBarRightRect.GetRectangle(), Color.Green);

			spriteBatch.Draw(healthBarTex, superBarRightRect.GetRectangle(), Color.Blue);
			spriteBatch.Draw(healthBarTex, superBarLeftRect.GetRectangle(), Color.Blue);
			if (DisplayComboDataP1) {
				spriteBatch.DrawString(font, comboCountP1.ToString(), new Vector2(-150, -150), Color.White, 0, Vector2.Zero, .25f, SpriteEffects.None, 0);
				spriteBatch.DrawString(font, comboDamageP1.ToString(), new Vector2(-150, -125), Color.White, 0, Vector2.Zero, .25f, SpriteEffects.None, 0);
			}
			if (DisplayComboDataP2) {
				spriteBatch.DrawString(font, comboCountP2.ToString(), new Vector2(150, -150), Color.White, 0, Vector2.Zero, .25f, SpriteEffects.None, 0);
				spriteBatch.DrawString(font, comboDamageP2.ToString(), new Vector2(150, -125), Color.White, 0, Vector2.Zero, .25f, SpriteEffects.None, 0);
			}
		}
		static public void HealthbarUpdate(float healthPercent, bool playerOne) {
			if (playerOne) {
				healthBarLeftRect.Width = (int)(healthBarWidth * healthPercent);
				//healthBarLeftRect = Transform.GetCustomRenderPosition(healthBarLeftRect, healthbarPositionLeft, TransformOriginPoint.right);
			} else {
				healthBarRightRect.Width = (int)(healthBarWidth * healthPercent);
				//healthBarRightRect = Transform.GetCustomRenderPosition(healthBarRightRect, healthBarPositionRight, TransformOriginPoint.left);

			}
		}
		static public void SuperbarUpdate(float superPercent, bool playerOne) {
			if (playerOne) {
				superBarLeftRect.Width = (int)(superMeterWidth * superPercent);
				//superBarLeftRect = Transform.GetCustomRenderPosition(superBarLeftRect, superBarPositionLeft, TransformOriginPoint.right);
			} else {
				superBarRightRect.Width = (int)(superMeterWidth * superPercent);
				//superBarRightRect = Transform.GetCustomRenderPosition(superBarRightRect, superBarPositionRight, TransformOriginPoint.left);

			}
		}
		static public void ComboTextUpdate(int comboDamage, int comboCount, bool playerOne) {
			if (playerOne) {
				comboCountP1 = comboCount;
				comboDamageP1 = comboDamage;
				DisplayComboDataP1 = true;
			} else {
				comboCountP2 = comboCount;
				comboDamageP2 = comboDamage;
				DisplayComboDataP2 = true;
			}

		}
		static public void HideComboText(bool playerOne) {
			if (playerOne) {
				comboFadeBufferP1 = 30;
			} else {
				comboFadeBufferP1 = 30;
			}
		}


		static public void Update() {
			if (comboFadeBufferP2 > 0) {
				comboFadeBufferP2--;
			} else {
				DisplayComboDataP2 = false;

			}

			if (comboFadeBufferP1 > 0) {
				comboFadeBufferP1--;
			} else {
				DisplayComboDataP1 = false;

			}

		}
	}
}
