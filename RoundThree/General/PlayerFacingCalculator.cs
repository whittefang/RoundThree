﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using EngineFang;

namespace RoundThree.General {
	static class PlayerFacingCalculator {
		private static Entity playerOne, playerTwo;

		public static void SetPlayer(int playerNumber, Entity player) {
			if (playerNumber == 1) {
				playerTwo = player;
			} else {
				playerOne = player;
			}
		}
		public static bool IsPlayerFacingRight(PlayerIndex index) {
			bool result = false;

			if (playerOne.transform.Position.X < playerTwo.transform.Position.X) {
				result = true;
			}

			if (index.Equals(PlayerIndex.One)) {
				return result;
			} else {
				return !result;
			}
		}
	}
}