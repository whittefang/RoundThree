﻿using EngineFang;
using RoundThree.General.Attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.General {
	static class PlayerSetter {

		public static void SetPlayer(int playerNumber, Entity entity) {
			HitResolver.SetPlayerHitResolver(playerNumber, entity.getComponent<PlayerHitResolver>());
			RoundHandler.SetPlayer(playerNumber, entity);
			PlayerFacingCalculator.SetPlayer(playerNumber, entity);
			PlayerCollisionResolver.SetPlayer(playerNumber, entity.getComponent<PlayerMover>());

		}
	}
}
