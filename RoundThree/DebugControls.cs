﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree {

	static class DebugControls{
		public static bool debugModeEnabled = true;
		public static Texture2D debugSquareTexture;
		public static bool pause, advanceOneFrameFlag;

		public static List<RenderableRectangleCollider> rectangles;

		static KeyboardState state, preState;


		static DebugControls() {
			rectangles = new List<RenderableRectangleCollider>();
		}

		public static void Update() {
			preState = state;
			state = Keyboard.GetState();
			if (advanceOneFrameFlag) {
				pause = true;
				advanceOneFrameFlag = false;
			}


			if (state.IsKeyDown(Keys.F1) && preState.IsKeyUp(Keys.F1)) {
				debugModeEnabled = !debugModeEnabled;
			}
			if (state.IsKeyDown(Keys.Space) && preState.IsKeyUp(Keys.Space)) {
				pause = !pause;
			}

			if (state.IsKeyDown(Keys.Right) && preState.IsKeyUp(Keys.Right)) {
				pause = false;
				advanceOneFrameFlag = true;
			}


		}
		public static void Load(ContentManager content) {
			debugSquareTexture = content.Load<Texture2D>("square");
		}

		public static void Draw(SpriteBatch spriteBatch) {
			if (debugModeEnabled) {
				foreach (RenderableRectangleCollider rect in rectangles) {
					spriteBatch.Draw(debugSquareTexture, rect.Collider.GetRectangle(), rect.Color);
				}
			}
		}
	}
}
