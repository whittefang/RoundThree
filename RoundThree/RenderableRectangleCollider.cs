﻿using Microsoft.Xna.Framework;
using RoundThree.EngineFang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree {
	class RenderableRectangleCollider {
		public RectangleCollider Collider{ get; set; }
		public Color Color { get; set; }

		public RenderableRectangleCollider(RectangleCollider collider, Color color) {
			Collider = collider;
			Color = color;
		}

	}
}
