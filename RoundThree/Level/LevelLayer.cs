﻿using EngineFang;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.Level {
	class LevelLayer : Updatable {
		Vector2 position;
		float translationFactor;
		List<Entity> entities;

		public LevelLayer(float translationFactor, List<Entity> entities) {
			this.translationFactor = translationFactor;
			this.entities = entities;
		}

		public void Update() {
			foreach (Entity e in entities) {
				e.Update();
			}
		}

		public void SetPosition(Vector2 newPosition) {
			position.X = newPosition.X * translationFactor;
			position.Y = newPosition.Y;
			foreach (Entity e in entities) {
				e.transform.Position = position;
			}
		}
	}
}
