﻿using EngineFang;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundThree.Level {
	class LevelBackground: Component, Updatable {
		List<LevelLayer> layers;

		public LevelBackground(List<LevelLayer> layers) {
			this.layers = layers;
		}

		public void Update() {
			transform.Position = new Vector2(Camera.Position.X, 165);

			foreach (LevelLayer layer in layers) {
				layer.SetPosition(transform.Position);
				layer.Update();
			}
		}
	}
}
